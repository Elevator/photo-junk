﻿namespace Elevator.PhotoJunk.Logic.Helpers
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class EntityFactory : IEntityFactory
	{
		private readonly IUserService _userService;
		private readonly IImageService _imageService;
		private readonly IPhotoService _photoService;
		private readonly IAlbumService _albumService;

		public EntityFactory(IUserService userService, IImageService imageService, IPhotoService photoService, IAlbumService albumService)
		{
			_userService = userService;
			_imageService = imageService;
			_photoService = photoService;
			_albumService = albumService;
		}

		public IUser GetUser(Guid id)
		{
			return _userService.GetUserById(id);
		}

		public IImage GetImage(Guid id)
		{
			return _imageService.GetImage(id);
		}

		public IPhoto GetPhoto(Guid id)
		{
			return _photoService.GetPhoto(id);
		}

		public IAlbum GetAlbum(Guid id)
		{
			return _albumService.GetAlbumById(id);
		}
	}
}