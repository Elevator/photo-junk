﻿namespace Elevator.PhotoJunk.Logic.Helpers
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;

	public interface IEntityFactory
	{
		IUser GetUser(Guid id);
		IImage GetImage(Guid id);
		IPhoto GetPhoto(Guid id);
		IAlbum GetAlbum(Guid id);
	}
}