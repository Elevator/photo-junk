﻿namespace Elevator.PhotoJunk.Logic.Helpers
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Entities.Proxies;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class ProxyFactory : IEntityFactory
	{
		private readonly IUserService _userService;
		private readonly IImageService _imageService;
		private readonly IPhotoService _photoService;
		private readonly IAlbumService _albumService;

		public ProxyFactory(IUserService userService, IImageService imageService, IPhotoService photoService,
			IAlbumService albumService)
		{
			_userService = userService;
			_imageService = imageService;
			_photoService = photoService;
			_albumService = albumService;
		}

		public IUser GetUser(Guid id)
		{
			return new UserProxy(id, _userService);
		}

		public IImage GetImage(Guid id)
		{
			return new ImageProxy(id, _imageService);
		}

		public IPhoto GetPhoto(Guid id)
		{
			return new PhotoProxy(id, _photoService);
		}

		public IAlbum GetAlbum(Guid id)
		{
			return new AlbumProxy(id, _albumService);
		}
	}
}