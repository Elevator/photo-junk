﻿namespace Elevator.PhotoJunk.Logic.Entities
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;

	public class Album : IAlbum
	{
		public Album(Guid idAlbum, IUser author, string title, string description, IImage cover, DateTime uploadDate)
		{
			IdAlbum = idAlbum;
			Author = author;
			Title = title;
			Description = description;
			Cover = cover;
			UploadDate = uploadDate;
		}

		public Guid IdAlbum { get; set; }
		public IUser Author { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public IImage Cover { get; set; }
		public DateTime UploadDate { get; set; }
	}
}