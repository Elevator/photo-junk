﻿namespace Elevator.PhotoJunk.Logic.Entities
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;

	public class User : IUser
	{
		public User(
			Guid idUser,
			string email,
			byte[] password,
			string nickname,
			string firstName,
			string lastName,
			string middleName,
			string location,
			string description,
			DateTime registrationDate,
			bool isPremium)
		{
			IdUser = idUser;
			Email = email;
			Password = password;
			Nickname = nickname;
			FirstName = firstName;
			LastName = lastName;
			MiddleName = middleName;
			Location = location;
			Description = description;
			RegistrationDate = registrationDate;
			IsPremium = isPremium;
		}


		public Guid IdUser { get; private set; }
		public string Email { get; set; }
		public byte[] Password { get; set; }
		public string Nickname { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleName { get; set; }
		public string Location { get; set; }
		public string Description { get; set; }
		public DateTime RegistrationDate { get; set; }
		public bool IsPremium { get; set; }
	}
}