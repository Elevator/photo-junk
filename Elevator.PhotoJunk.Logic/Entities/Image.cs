﻿namespace Elevator.PhotoJunk.Logic.Entities
{
	using System;
	using SysImage = System.Drawing.Image;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;

	public class Image : IImage
	{
		public Image(Guid idImage, int width, int height, string mimeType, string fileExtension)
		{
			IdImage = idImage;
			Width = width;
			Height = height;
			MimeType = mimeType;
			FileExtension = fileExtension;
		}

		public Guid IdImage { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public string MimeType { get; set; }
		public string FileExtension { get; set; }
	}
}