﻿namespace Elevator.PhotoJunk.Logic.Entities
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;

	public class Photo : IPhoto
	{
		public Photo(
			Guid idPhoto, 
			string title,
			string description,
			string place,
			string camera,
			double focalLength,
			double apertureSize,
			int isoSpeed,
			bool flash,
			IUser author,
			IImage image,
			DateTime uploadDate)
		{
			IdPhoto = idPhoto;
			Title = title;
			Description = description;
			Place = place;
			Camera = camera;
			FocalLength = focalLength;
			ApertureSize = apertureSize;
			IsoSpeed = isoSpeed;
			Flash = flash;
			Author = author;
			Image = image;
			UploadDate = uploadDate;
		}


		public Guid IdPhoto { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Place { get; set; }
		public string Camera { get; set; }
		public double FocalLength { get; set; }
		public double ApertureSize { get; set; }
		public int IsoSpeed { get; set; }
		public bool Flash { get; set; }
		public IUser Author { get; set; }
		public IImage Image { get; set; }
		public DateTime UploadDate { get; set; }
	}
}