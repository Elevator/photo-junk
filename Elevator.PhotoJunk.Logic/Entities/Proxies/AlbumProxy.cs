﻿namespace Elevator.PhotoJunk.Logic.Entities.Proxies
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class AlbumProxy : Proxy<Album>, IAlbum
	{
		private readonly Guid _idAlbum;
		private readonly IAlbumService _albumService;

		public AlbumProxy(Guid idAlbum, IAlbumService albumService)
		{
			_idAlbum = idAlbum;
			_albumService = albumService;
		}

		public Guid IdAlbum
		{
			get { return _idAlbum; }
		}

		public IUser Author
		{
			get { return Instance.Author; }
			set { Instance.Author = value; }
		}

		public string Title
		{
			get { return Instance.Title; }
			set { Instance.Title = value; }
		}

		public string Description
		{
			get { return Instance.Description; }
			set { Instance.Description = value; }
		}

		public IImage Cover
		{
			get { return Instance.Cover; }
			set { Instance.Cover = value; }
		}

		public DateTime UploadDate
		{
			get { return Instance.UploadDate; }
			set { Instance.UploadDate = value; }
		}

		protected override Album GetInstance()
		{
			return _albumService.GetAlbumById(_idAlbum);
		}
	}
}