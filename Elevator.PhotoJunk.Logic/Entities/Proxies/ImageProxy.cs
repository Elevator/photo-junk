﻿namespace Elevator.PhotoJunk.Logic.Entities.Proxies
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class ImageProxy : Proxy<Image>, IImage
	{
		private readonly Guid _idImage;
		private readonly IImageService _imageService;

		public ImageProxy(Guid idImage, IImageService imageService)
		{
			_idImage = idImage;
			_imageService = imageService;
		}

		public Guid IdImage
		{
			get { return _idImage; }
		}

		public int Width
		{
			get { return Instance.Width; }
			set { Instance.Width = value; }
		}

		public int Height
		{
			get { return Instance.Height; }
			set { Instance.Height = value; }
		}

		public string MimeType
		{
			get { return Instance.MimeType; }
			set { Instance.MimeType = value; }
		}

		public string FileExtension
		{
			get { return Instance.FileExtension; }
			set { Instance.FileExtension = value; }
		}

		protected override Image GetInstance()
		{
			return _imageService.GetImage(_idImage);
		}
	}
}