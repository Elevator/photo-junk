﻿namespace Elevator.PhotoJunk.Logic.Entities.Proxies
{
	public abstract class Proxy<T> where T : class
	{
		private T _instance;

		protected T Instance
		{
			get { return _instance ?? (_instance = GetInstance()); }
		}

		protected abstract T GetInstance();
	}
}