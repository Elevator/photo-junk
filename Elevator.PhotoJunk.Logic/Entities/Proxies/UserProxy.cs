﻿namespace Elevator.PhotoJunk.Logic.Entities.Proxies
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class UserProxy : Proxy<User>, IUser
	{
		private readonly IUserService _userService;
		private readonly Guid _idUser;

		public UserProxy(Guid idUser, IUserService userService)
		{
			_idUser = idUser;
			_userService = userService;
		}

		public Guid IdUser
		{
			get { return _idUser; }
		}

		public string Email
		{
			get { return Instance.Email; }
			set { Instance.Email = value; }
		}

		public byte[] Password
		{
			get { return Instance.Password; }
			set { Instance.Password = value; }
		}

		public string Nickname
		{
			get { return Instance.Nickname; }
			set { Instance.Nickname = value; }
		}

		public string FirstName
		{
			get { return Instance.FirstName; }
			set { Instance.FirstName = value; }
		}

		public string LastName
		{
			get { return Instance.LastName; }
			set { Instance.LastName = value; }
		}

		public string MiddleName
		{
			get { return Instance.MiddleName; }
			set { Instance.MiddleName = value; }
		}

		public string Location
		{
			get { return Instance.Location; }
			set { Instance.Location = value; }
		}

		public string Description
		{
			get { return Instance.Description; }
			set { Instance.Description = value; }
		}

		public DateTime RegistrationDate
		{
			get { return Instance.RegistrationDate; }
			set { Instance.RegistrationDate = value; }
		}

		public bool IsPremium
		{
			get { return Instance.IsPremium; }
			set { Instance.IsPremium = value; }
		}

		protected override User GetInstance()
		{
			return _userService.GetUserById(_idUser);
		}
	}
}