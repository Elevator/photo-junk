﻿namespace Elevator.PhotoJunk.Logic.Entities.Proxies
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class PhotoProxy : Proxy<Photo>, IPhoto
	{
		private readonly IPhotoService _photoService;
		private readonly Guid _idPhoto;

		public PhotoProxy(Guid idPhoto, IPhotoService photoService)
		{
			_idPhoto = idPhoto;
			_photoService = photoService;
		}

		public Guid IdPhoto
		{
			get { return _idPhoto; }
		}

		public string Title
		{
			get { return Instance.Title; }
			set { Instance.Title = value; }
		}

		public string Description
		{
			get { return Instance.Description; }
			set { Instance.Description = value; }
		}

		public string Place
		{
			get { return Instance.Place; }
			set { Instance.Place = value; }
		}

		public string Camera
		{
			get { return Instance.Camera; }
			set { Instance.Camera = value; }
		}

		public double FocalLength
		{
			get { return Instance.FocalLength; }
			set { Instance.FocalLength = value; }
		}

		public double ApertureSize
		{
			get { return Instance.ApertureSize; }
			set { Instance.ApertureSize = value; }
		}

		public int IsoSpeed
		{
			get { return Instance.IsoSpeed; }
			set { Instance.IsoSpeed = value; }
		}

		public bool Flash
		{
			get { return Instance.Flash; }
			set { Instance.Flash = value; }
		}

		public IUser Author
		{
			get { return Instance.Author; }
			set { Instance.Author = value; }
		}

		public IImage Image
		{
			get { return Instance.Image; }
			set { Instance.Image = value; }
		}

		public DateTime UploadDate
		{
			get { return Instance.UploadDate; }
			set { Instance.UploadDate = value; }
		}
		
		protected override Photo GetInstance()
		{
			return _photoService.GetPhoto(_idPhoto);
		}


	}
}