﻿namespace Elevator.PhotoJunk.Logic.Entities.Abstract
{
	using System;

	public interface IAlbum
	{
		Guid IdAlbum { get; }
		IUser Author { get; set; }
		string Title { get; set; }
		string Description { get; set; }
		IImage Cover { get; set; }
		DateTime UploadDate { get; set; }
	}
}