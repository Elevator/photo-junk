﻿namespace Elevator.PhotoJunk.Logic.Entities.Abstract
{
	using System;

	public interface IUser
	{
		Guid IdUser { get; }
		string Email { get; set; }
		byte[] Password { get; set; }
		string Nickname { get; set; }
		string FirstName { get; set; }
		string LastName { get; set; }
		string MiddleName { get; set; }
		string Location { get; set; }
		string Description { get; set; }
		DateTime RegistrationDate { get; set; }
		bool IsPremium { get; set; }
	}
}