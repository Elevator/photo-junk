﻿namespace Elevator.PhotoJunk.Logic.Entities.Abstract
{
	using System;
	using SysImage = System.Drawing.Image;

	public interface IImage
	{
		Guid IdImage { get; }
		int Width { get; set; }
		int Height { get; set; }
		string MimeType { get; set; }
		string FileExtension { get; set; }
	}
}