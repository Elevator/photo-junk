﻿namespace Elevator.PhotoJunk.Logic.Entities.Abstract
{
	using System;

	public interface IPhoto
	{
		Guid IdPhoto { get; }
		string Title { get; set; }
		string Description { get; set; }
		string Place { get; set; }
		string Camera { get; set; }
		double FocalLength { get; set; }
		double ApertureSize { get; set; }
		int IsoSpeed { get; set; }
		bool Flash { get; set; }
		IUser Author { get; set; }
		IImage Image { get; set; }
		DateTime UploadDate { get; set; }
	}
}