﻿namespace Elevator.PhotoJunk.Logic.Pagination
{
	using System.Linq;
	using AutoMapper;

	public static class PagedMapper
	{
		public static PagedData<TDestination> Map<TSource, TDestination>(PagedData<TSource> sourceData)
		{
			return new PagedData<TDestination>(sourceData.Data.Select(Mapper.Map<TSource, TDestination>), sourceData.TotalItems, sourceData.ItemsPerPage, sourceData.CurrentPage);
		}
	}
}