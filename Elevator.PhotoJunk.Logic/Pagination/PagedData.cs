﻿namespace Elevator.PhotoJunk.Logic.Pagination
{
	using System;
	using System.Collections;
	using System.Collections.Generic;

	public class PagedData<T> : IPaged, IEnumerable<T>
	{
		public PagedData(IEnumerable<T> data, int totalItems, int itemsPerPage, int currentPage)
		{
			Data = data;
			TotalItems = totalItems;
			ItemsPerPage = itemsPerPage;
			NumberOfPages = GetNumberOfPages(ItemsPerPage, TotalItems);
			CurrentPage = currentPage;
		}

		public IEnumerable<T> Data { get; private set; }
		public int NumberOfPages { get; private set; }
		public int TotalItems { get; private set; }
		public int ItemsPerPage { get; private set; }
		public int CurrentPage { get; private set; }

		public IEnumerator<T> GetEnumerator()
		{
			return Data.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return Data.GetEnumerator();
		}

		public static int GetNumberOfPages(int itemsPerPage, int itemsTotal)
		{
			return (int) Math.Ceiling((double) itemsTotal/itemsPerPage);
		}
	}
}