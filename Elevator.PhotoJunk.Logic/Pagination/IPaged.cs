﻿namespace Elevator.PhotoJunk.Logic.Pagination
{
	public interface IPaged
	{
		int NumberOfPages { get; }
		int TotalItems { get; }
		int ItemsPerPage { get; }
		int CurrentPage { get; }
	}
}