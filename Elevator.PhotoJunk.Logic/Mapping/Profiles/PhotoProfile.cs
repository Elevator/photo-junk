﻿namespace Elevator.PhotoJunk.Logic.Mapping.Profiles
{
	using System;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Helpers;
	using Microsoft.Practices.ServiceLocation;
	using PhotoDto = Elevator.PhotoJunk.Data.Core.Dto.Photo;

	public class PhotoProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<PhotoDto, Photo>().ConstructUsing(
				source => new Photo(
					source.IdPhoto,
					source.Title,
					source.Description,
					source.Place,
					source.Camera,
					source.FocalLength,
					source.ApertureSize,
					source.IsoSpeed,
					source.Flash,
					GetUser(source.IdAuthor),
					GetImage(source.IdImage),
					source.UploadDate))
				.ForMember(d => d.Author, m => m.ResolveUsing(s => GetUser(s.IdAuthor)))
				.ForMember(d => d.Image, m => m.ResolveUsing(s => GetImage(s.IdImage)));

			Mapper.CreateMap<IPhoto, PhotoDto>()
				.ForMember(x => x.IdPhoto, m => m.MapFrom(s => s.IdPhoto))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.Place, m => m.MapFrom(s => s.Place))
				.ForMember(x => x.Camera, m => m.MapFrom(s => s.Camera))
				.ForMember(x => x.FocalLength, m => m.MapFrom(s => s.FocalLength))
				.ForMember(x => x.ApertureSize, m => m.MapFrom(s => s.ApertureSize))
				.ForMember(x => x.IsoSpeed, m => m.MapFrom(s => s.IsoSpeed))
				.ForMember(x => x.Flash, m => m.MapFrom(s => s.Flash))
				.ForMember(x => x.IdAuthor, m => m.MapFrom(s => s.Author.IdUser))
				.ForMember(x => x.IdImage, m => m.MapFrom(s => s.Image.IdImage))
				.ForMember(x => x.UploadDate, m => m.MapFrom(s => s.UploadDate));
		}

		private static IUser GetUser(Guid id)
		{
			return ServiceLocator.Current.GetInstance<IEntityFactory>().GetUser(id);
		}

		private static IImage GetImage(Guid id)
		{
			return ServiceLocator.Current.GetInstance<IEntityFactory>().GetImage(id);
		}
	}
}