﻿namespace Elevator.PhotoJunk.Logic.Mapping.Profiles
{
	using System;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Helpers;
	using Microsoft.Practices.ServiceLocation;
	using AlbumDto = Elevator.PhotoJunk.Data.Core.Dto.Album;

	public class AlbumProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<AlbumDto, Album>().ConstructUsing(
				source => new Album(
					source.IdAlbum,
					GetUser(source.IdAuthor),
					source.Title,
					source.Description,
					GetImage(source.IdCover),
					source.UploadDate))
				.ForMember(d => d.Author, m => m.ResolveUsing(s => GetUser(s.IdAuthor)))
				.ForMember(d => d.Cover, m => m.ResolveUsing(s => GetImage(s.IdCover)));

			Mapper.CreateMap<IAlbum, AlbumDto>()
				.ForMember(x => x.IdAlbum, m => m.MapFrom(s => s.IdAlbum))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.IdAuthor, m => m.MapFrom(s => s.Author.IdUser))
				.ForMember(x => x.IdCover, m => m.MapFrom(s => s.Cover.IdImage))
				.ForMember(x => x.UploadDate, m => m.MapFrom(s => s.UploadDate));
		}

		private static IUser GetUser(Guid id)
		{
			return ServiceLocator.Current.GetInstance<IEntityFactory>().GetUser(id);
		}

		private static IImage GetImage(Guid id)
		{
			return ServiceLocator.Current.GetInstance<IEntityFactory>().GetImage(id);
		}
	}
}