﻿namespace Elevator.PhotoJunk.Logic.Mapping
{
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Mapping.Profiles;

	public static class AutomapperLogicConfiguration
	{
		public static void Configure(IConfiguration configuration)
		{
			configuration.AddProfile(new UserProfile());
			configuration.AddProfile(new PhotoProfile());
			configuration.AddProfile(new AlbumProfile());
		}
	}
}