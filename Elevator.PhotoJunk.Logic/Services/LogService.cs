﻿namespace Elevator.PhotoJunk.Logic.Services
{
	using System;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class LogService : ILogService
	{
		private readonly ILogRepository _logRepository;

		public LogService(ILogRepository logRepository)
		{
			_logRepository = logRepository;
		}

		public void WiteToLog(LogLevel level, DateTime timeStamp, string source, string message)
		{
			_logRepository.WiteToLog(level.ToString(), timeStamp, source, message);
		}
	}
}