﻿namespace Elevator.PhotoJunk.Logic.Services
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using AlbumDto = Elevator.PhotoJunk.Data.Core.Dto.Album;

	public class AlbumService : IAlbumService
	{
		private readonly IAlbumRepository _albumRepository;

		public AlbumService(IAlbumRepository albumRepository)
		{
			_albumRepository = albumRepository;
		}


		public IAlbum SaveAlbum(IAlbum album)
		{
			AlbumDto dto = Mapper.Map<AlbumDto>(album);
			dto = _albumRepository.Create(dto);
			return Mapper.Map<Album>(dto);
		}

		public void UpdateAlbum(IAlbum album)
		{
			AlbumDto albumDto = _albumRepository.GetById(album.IdAlbum);
			albumDto = Mapper.Map(album, albumDto);
			_albumRepository.Update(albumDto);
		}

		public void DeleteAlbum(Guid id)
		{
			_albumRepository.Delete(id);
		}

		public Album GetAlbumById(Guid id)
		{
			AlbumDto album = _albumRepository.GetById(id);
			return album != null ? Mapper.Map<Album>(album) : null;
		}

		public Album GetAlbumByTitle(string title)
		{
			AlbumDto album = _albumRepository.GetByTitle(title);
			return album != null ? Mapper.Map<Album>(album) : null;
		}


		public PagedData<Album> GetAlbums(int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Album> albums = _albumRepository.GetPage(pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Album>);
			return new PagedData<Album>(albums, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Album> FilterAlbumsByAuthor(Guid userId, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Album> albums = _albumRepository.FilterByAuthor(userId, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Album>);
			return new PagedData<Album>(albums, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Album> FilterAlbumsByKeywords(string keyWords, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Album> albums = _albumRepository.Filter(keyWords, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Album>);
			return new PagedData<Album>(albums, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Album> FilterAlbums(string title, string descriprion, string authorName, int pageNumber,
			int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Album> albums = _albumRepository.Filter(title, descriprion, authorName, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Album>);
			return new PagedData<Album>(albums, itemsTotal, itemsPerPage, pageNumber);
		}
	}
}
