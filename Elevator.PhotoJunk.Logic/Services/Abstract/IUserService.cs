﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;

	public interface IUserService
	{
		User ConstructUser(string email, string name, string password);
		IUser CreateUser(IUser user);
		bool UpdateUser(IUser user);
		bool RemoveUser(string email);
		User GetUserByEmail(string email);
		User GetUserByNickname(string name);
		User GetUserById(Guid id);

		PagedData<User> GetUsers(int pageNumber, int itemsPerPage);
		PagedData<User> FilterUsersByKeywords(string keyWords, int pageNumber, int itemsPerPage);

		PagedData<User> FilterUsers(string email, string nickname, string firstName, string middleName, string lastName,
			string location, string description, DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd,
			bool? premium, int pageNumber, int itemsPerPage);

		PagedData<User> FilterUsersByAnyName(string email, string anyName, string location, string description,
			DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd, bool? premium,
			int pageNumber, int itemsPerPage);

		bool UserHasAvatar(Guid id);
		Guid GetAvatarImageId(Guid id);
		void SetAvatar(Guid userId, Guid imageId);
		void RemoveAvatar(Guid userId);

		User Login(string userName, string password);
		bool ChangePassword(IUser user, string oldPassword, string newPassword);
	}
}