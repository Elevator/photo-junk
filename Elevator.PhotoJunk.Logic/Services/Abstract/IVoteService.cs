﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	using System;

	public interface IVoteService
	{
		void VotePhoto(Guid userId, Guid photoId, Vote vote);

		int GetLikesForPhoto(Guid photoId);
		int GetDislikesForPhoto(Guid photoId);

		bool UserCanVote(Guid userId, Guid photoId);
		Vote GetUserVote(Guid userId, Guid photoId);
	}
}