﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	public enum Vote
	{
		Unknown,
		Like,
		Dislike
	}
}