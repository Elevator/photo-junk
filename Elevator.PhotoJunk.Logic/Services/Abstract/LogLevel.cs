﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	public enum LogLevel
	{
		Fatal,
		Error,
		Warn,
		Info,
		Debug
	}
}