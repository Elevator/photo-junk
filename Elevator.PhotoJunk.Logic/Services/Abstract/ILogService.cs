﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	using System;

	public interface ILogService
	{
		void WiteToLog(LogLevel level, DateTime timeStamp, string source, string message);
	}
}