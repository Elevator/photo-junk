﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;

	public interface IAlbumService
	{
		IAlbum SaveAlbum(IAlbum album);
		void UpdateAlbum(IAlbum album);
		void DeleteAlbum(Guid id);
		Album GetAlbumById(Guid id);
		Album GetAlbumByTitle(string title);

		PagedData<Album> GetAlbums(int pageNumber, int itemsPerPage);
		PagedData<Album> FilterAlbumsByAuthor(Guid userId, int pageNumber, int itemsPerPage);
		PagedData<Album> FilterAlbumsByKeywords(string keyWords, int pageNumber, int itemsPerPage);
		PagedData<Album> FilterAlbums(string title, string descriprion, string authorName, int pageNumber, int itemsPerPage);
	}
}