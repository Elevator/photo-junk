﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;

	public interface IImageService
	{
		IImage SaveImage(IImage image, byte[] rawData);
		void DeleteImage(Guid id);
		Image GetImage(Guid id);
		byte[] GetRawData(Guid id);
	}
}