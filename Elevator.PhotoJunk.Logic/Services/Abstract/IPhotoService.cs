﻿namespace Elevator.PhotoJunk.Logic.Services.Abstract
{
	using System;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;

	public interface IPhotoService
	{
		IPhoto SavePhoto(IPhoto photo);
		void UpdatePhoto(IPhoto photo);
		void DeletePhoto(Guid id);
		Photo GetPhoto(Guid id);

		PagedData<Photo> GetPhotos(int pageNumber, int itemsPerPage);
		PagedData<Photo> FilterPhotosByAuthor(Guid userId, int pageNumber, int itemsPerPage);
		PagedData<Photo> FilterPhotosByAlbum(Guid albumId, int pageNumber, int itemsPerPage);
		PagedData<Photo> FilterPhotosByKeywords(string keyWords, int pageNumber, int itemsPerPage);

		PagedData<Photo> FilterPhotos(string title, string descriprion, string place, string camera, string authorName,
			int pageNumber, int itemsPerPage);

		void AddPhotoToAlbum(Guid photoId, Guid albumId);
		void RemovePhotoFromAlbum(Guid photoId, Guid albumId);
	}
}