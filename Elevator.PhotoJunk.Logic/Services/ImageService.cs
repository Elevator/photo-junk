﻿namespace Elevator.PhotoJunk.Logic.Services
{
	using System;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using SysImage = System.Drawing.Image;
	using ImageDto = Elevator.PhotoJunk.Data.Core.Dto.Image;

	public class ImageService : IImageService
	{
		private readonly IImageRepository _imageRepository;

		public ImageService(IImageRepository imageRepository)
		{
			_imageRepository = imageRepository;
		}

		public IImage SaveImage(IImage image, byte[] rawData)
		{
			ImageDto dto = new ImageDto
			{
				IdImage = image.IdImage,
				Width = image.Width,
				Height = image.Height,
				MimeType = image.MimeType,
				FileExtension = image.FileExtension
			};

			dto = _imageRepository.Create(dto, rawData);

			return new Image(dto.IdImage, dto.Width, dto.Height, dto.MimeType, dto.FileExtension);
		}

		public void DeleteImage(Guid id)
		{
			_imageRepository.Delete(id);
		}

		public Image GetImage(Guid id)
		{
			ImageDto image = _imageRepository.GetById(id);
			return image != null
				? new Image(image.IdImage, image.Width, image.Height, image.MimeType, image.FileExtension)
				: null;
		}

		public byte[] GetRawData(Guid id)
		{
			return _imageRepository.GetRawDataById(id);
		}
	}
}