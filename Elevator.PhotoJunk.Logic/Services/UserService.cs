﻿namespace Elevator.PhotoJunk.Logic.Services
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Security.Cryptography;
	using System.Text;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using UserDto = Elevator.PhotoJunk.Data.Core.Dto.User;
	using ImageDto = Elevator.PhotoJunk.Data.Core.Dto.Image;

	public class UserService : IUserService
	{
		private readonly IUserRepository _userRepository;

		public UserService(IUserRepository userRepository)
		{
			_userRepository = userRepository;
		}

		public User ConstructUser(string email, string name, string password)
		{
			return new User(Guid.Empty, email, GetPasswordHash(password), name, "", "", "", "", "", DateTime.Now, false);
		}

		public IUser CreateUser(IUser user)
		{
			UserDto dto = Mapper.Map<UserDto>(user);
			dto = _userRepository.Create(dto);
			return Mapper.Map<User>(dto);
		}

		public bool UpdateUser(IUser user)
		{
			UserDto userDto = _userRepository.GetById(user.IdUser);

			if (userDto == null) return false;

			userDto = Mapper.Map(user, userDto);

			_userRepository.Update(userDto);
			return true;
		}

		public bool RemoveUser(string email)
		{
			UserDto userDto = _userRepository.GetByEmail(email);

			if (userDto == null) return false;

			_userRepository.Delete(userDto.Email);

			return true;
		}

		public User GetUserByEmail(string email)
		{
			UserDto user = _userRepository.GetByEmail(email);
			return user != null ? Mapper.Map<User>(user) : null;
		}

		public User GetUserByNickname(string name)
		{
			UserDto user = _userRepository.GetByNickname(name);
			return user != null ? Mapper.Map<User>(user) : null;
		}

		public User GetUserById(Guid id)
		{
			UserDto user = _userRepository.GetById(id);
			return user != null ? Mapper.Map<User>(user) : null;
		}

		public PagedData<User> GetUsers(int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<User> users = _userRepository.GetPage(pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<User>);
			return new PagedData<User>(users, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<User> FilterUsers(string email, string nickname, string firstName, string middleName, string lastName,
			string location, string description, DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd,
			bool? premium, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<User> users =
				_userRepository.Filter(email, nickname, firstName, middleName, lastName, location, description, registrationPeriodStart, registrationPeriodEnd,
					premium, pageNumber,
					itemsPerPage, out itemsTotal).Select(Mapper.Map<User>);
			return new PagedData<User>(users, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<User> FilterUsersByAnyName(string email, string anyName, string location, string description,
			DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd, bool? premium, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<User> users =
				_userRepository.FilterByAnyName(email, anyName, location, description, registrationPeriodStart, registrationPeriodEnd, premium, pageNumber,
					itemsPerPage,
					out itemsTotal).Select(Mapper.Map<User>);
			return new PagedData<User>(users, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<User> FilterUsersByKeywords(string keyWords, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<User> users =
				_userRepository.FilterByKeywords(keyWords, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<User>);
			return new PagedData<User>(users, itemsTotal, itemsPerPage, pageNumber);
		}

		public bool UserHasAvatar(Guid userId)
		{
			return _userRepository.UserAvatarExist(userId);
		}

		public Guid GetAvatarImageId(Guid userId)
		{
			return _userRepository.GetUserAvatar(userId);
		}

		public void SetAvatar(Guid userId, Guid imageId)
		{
			_userRepository.SetUserAvatar(userId, imageId);
		}

		public void RemoveAvatar(Guid userId)
		{
			_userRepository.RemoveUserAvatar(userId);
		}

		public User Login(string userName, string password)
		{
			byte[] passwordHash = GetPasswordHash(password);

			UserDto userDto = _userRepository.GetByNickname(userName);
			if (userDto == null) return null;

			if (CompareHashes(passwordHash, userDto.Password))
				return Mapper.Map<User>(userDto);

			return null;
		}

		public bool ChangePassword(IUser user, string oldPassword, string newPassword)
		{
			byte[] oldPasswordHash = GetPasswordHash(oldPassword);
			byte[] newPasswordHash = GetPasswordHash(newPassword);

			UserDto userDto = _userRepository.GetByEmail(user.Email);
			if (userDto == null) return false;

			if (CompareHashes(oldPasswordHash, userDto.Password))
			{
				userDto.Password = newPasswordHash;
				user.Password = newPasswordHash;
				_userRepository.Update(userDto);
				return true;
			}
			return false;
		}

		private static bool CompareHashes(byte[] hash1, byte[] hash2)
		{
			int count1 = hash1.Count();
			int count2 = hash2.Count();

			if (count1 != count2) return false;

			for (int i = 0; i < count1; ++i)
			{
				if (hash1[i] != hash2[i])
					return false;
			}
			return true;
		}

		private static byte[] GetPasswordHash(string password)
		{
			SHA256 sha = SHA256.Create();

			byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
			return sha.ComputeHash(passwordBytes);
		}
	}
}