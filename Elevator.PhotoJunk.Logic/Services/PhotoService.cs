﻿namespace Elevator.PhotoJunk.Logic.Services
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using PhotoDto = Elevator.PhotoJunk.Data.Core.Dto.Photo;

	public class PhotoService : IPhotoService
	{
		private readonly IPhotoRepository _photoRepository;

		public PhotoService(IPhotoRepository photoRepository)
		{
			_photoRepository = photoRepository;
		}

		public IPhoto SavePhoto(IPhoto photo)
		{
			PhotoDto photoDto = Mapper.Map<PhotoDto>(photo);
			photoDto = _photoRepository.Create(photoDto);
			return Mapper.Map<Photo>(photoDto);
		}

		public void UpdatePhoto(IPhoto photo)
		{
			PhotoDto photoDto = _photoRepository.GetById(photo.IdPhoto);
			photoDto = Mapper.Map(photo, photoDto);
			_photoRepository.Update(photoDto);
		}

		public void DeletePhoto(Guid id)
		{
			_photoRepository.Delete(id);
		}

		public Photo GetPhoto(Guid id)
		{
			PhotoDto photo = _photoRepository.GetById(id);
			return photo != null ? Mapper.Map<Photo>(photo) : null;
		}

		public PagedData<Photo> GetPhotos(int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Photo> photos = _photoRepository.GetPage(pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Photo>);
			return new PagedData<Photo>(photos, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Photo> FilterPhotosByAuthor(Guid userId, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Photo> photos = _photoRepository.FilterByAuthor(userId, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Photo>);
			return new PagedData<Photo>(photos, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Photo> FilterPhotosByAlbum(Guid albumId, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Photo> photos = _photoRepository.FilterByAlbum(albumId, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Photo>);
			return new PagedData<Photo>(photos, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Photo> FilterPhotosByKeywords(string keyWords, int pageNumber, int itemsPerPage)
		{
			int itemsTotal; 
			IEnumerable<Photo> photos = _photoRepository.FilterByKeywords(keyWords, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Photo>);
			return new PagedData<Photo>(photos, itemsTotal, itemsPerPage, pageNumber);
		}

		public PagedData<Photo> FilterPhotos(string title, string descriprion, string place, string camera,
			string authorName, int pageNumber, int itemsPerPage)
		{
			int itemsTotal;
			IEnumerable<Photo> photos = _photoRepository.Filter(title, descriprion, place, camera, authorName, pageNumber, itemsPerPage, out itemsTotal).Select(Mapper.Map<Photo>);
			return new PagedData<Photo>(photos, itemsTotal, itemsPerPage, pageNumber);
		}

		public void AddPhotoToAlbum(Guid photoId, Guid albumId)
		{
			_photoRepository.AddPhotoToAlbum(photoId, albumId);	
		}

		public void RemovePhotoFromAlbum(Guid photoId, Guid albumId)
		{
			_photoRepository.RemovePhotoFromAlbum(photoId, albumId);	
		}
	}
}