﻿namespace Elevator.PhotoJunk.Logic.Services
{
	using System;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using PhotoDto = Elevator.PhotoJunk.Data.Core.Dto.Photo;

	public class VoteService : IVoteService
	{
		private readonly IVoteRepository _voteRepository;
		private readonly IPhotoRepository _photoRepository;

		public VoteService(IVoteRepository voteRepository, IPhotoRepository photoRepository)
		{
			_voteRepository = voteRepository;
			_photoRepository = photoRepository;
		}

		public void VotePhoto(Guid userId, Guid photoId, Vote vote)
		{
			_voteRepository.Vote(userId, photoId, VoteToInt(vote));
		}

		public int GetLikesForPhoto(Guid photoId)
		{
			return _voteRepository.GetPositiveVotesForPhoto(photoId);
		}

		public int GetDislikesForPhoto(Guid photoId)
		{
			return -_voteRepository.GetNegativeVotesForPhoto(photoId); //We want to see just number of negative votes, not a sum of them, which is negative
		}

		public bool UserCanVote(Guid userId, Guid photoId)
		{
			int vote = _voteRepository.GetVote(userId, photoId);
			PhotoDto photo = _photoRepository.GetById(photoId);
			return vote == 0 && photo.IdAuthor != userId;
		}

		public Vote GetUserVote(Guid userId, Guid photoId)
		{
			int vote = _voteRepository.GetVote(userId, photoId);
			return IntToVote(vote);
		}

		private int VoteToInt(Vote vote)
		{
			switch (vote)
			{
				case Vote.Like:
					return 1;
				case Vote.Dislike:
					return -1;
				default:
					return 0;
			}
		}

		private Vote IntToVote(int vote)
		{
			switch (vote)
			{
				case 1:
					return Vote.Like;
				case -1:
					return Vote.Dislike;
				default:
					return Vote.Unknown;
			}
		}
	}
}