﻿namespace Elevator.PhotoJunk.Site.Controllers
{
	using System;
	using System.Web.Mvc;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Filters;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Auth;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Elevator.PhotoJunk.Site.Models.User;
	using Ninject;

	[Authorize]
	[HandleExceptionFilter(View = "ErrorServer")]
	public class UserController : Controller
	{
		[Inject]
		public IAlbumService AlbumService { get; set; }

		[Inject]
		public IPhotoService PhotoService { get; set; }

		[Inject]
		public IUserService UserService { get; set; }

		[Inject]
		public IImageService ImageService { get; set; }

		[Inject]
		public ImageHelper ImageHelper { get; set; }

		[HttpGet]
		public ActionResult Index()
		{
			PagedData<UserThumbnail> thumbnails = GetIndexPage("", 0);
			return View(thumbnails);
		}

		[HttpPost]
		public ActionResult IndexPage(string keyWords, int page)
		{
			PagedData<UserThumbnail> thumbnails = GetIndexPage(keyWords, page);
			return PartialView("UsersListPartial", thumbnails);
		}

		private PagedData<UserThumbnail> GetIndexPage(string keyWords, int page)
		{
			int numberOfThumbnails = ConfigHelper.NumberOfThumbnailsForSpecializedPage;

			PagedData<User> users = string.IsNullOrEmpty(keyWords)
				? UserService.GetUsers(0, 500)
				: UserService.FilterUsersByKeywords(keyWords, page, numberOfThumbnails);

			return PagedMapper.Map<User, UserThumbnail>(users);
		}

		[HttpGet]
		public ActionResult Search()
		{
			UserSearchModel model = new UserSearchModel
			{
				RegistrationPeriodStart = DateTime.Now.AddYears(-1),
				RegistrationPeriodEnd = DateTime.Now
			};
			return View(model);
		}

		[HttpPost]
		public ActionResult SearchPage(UserSearchModel model, int page)
		{
			PagedData<User> usersByAnyName = UserService.FilterUsersByAnyName(model.Email, model.Name, model.Location,
				model.Description, model.RegistrationPeriodStart, model.RegistrationPeriodEnd, model.IsPremium ? true : (bool?) null, page,
				ConfigHelper.NumberOfThumbnailsForSpecializedPage);

			PagedData<UserThumbnail> thumbnails = PagedMapper.Map<User, UserThumbnail>(usersByAnyName);
			return PartialView("UsersListPartial", thumbnails);
		}

		[HttpGet]
		public ActionResult Show(string name)
		{
			User user = UserService.GetUserByNickname(name);
			UserShowModel model = Mapper.Map<UserShowModel>(user);
			return View(model);
		}

		[HttpPost]
		public ActionResult PhotosByAuthorPage(Guid userId, int page)
		{
			PagedData<Photo> photos = PhotoService.FilterPhotosByAuthor(userId, page,
				ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<PhotoThumbnail> thumbnails = PagedMapper.Map<Photo, PhotoThumbnail>(photos);

			return PartialView("PhotosAnonymousListPartial", thumbnails);
		}

		[HttpPost]
		public ActionResult AlbumsByAuthorPage(Guid userId, int page)
		{
			PagedData<Album> albums = AlbumService.FilterAlbumsByAuthor(userId, page,
				ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<AlbumThumbnail> thumbnails = PagedMapper.Map<Album, AlbumThumbnail>(albums);

			return PartialView("AlbumsAnonymousListPartial", thumbnails);
		}

		[HttpGet]
		public ActionResult Manage(ManageMessage? message)
		{
			ViewBag.StatusMessage =
				message == ManageMessage.ChangePasswordSuccess
					? "Your password has been changed."
					: message == ManageMessage.SetPasswordSuccess
						? "Your password has been set."
						: message == ManageMessage.RemoveLoginSuccess
							? "The external login was removed."
							: "";
			ViewBag.ReturnUrl = Url.Action("Manage");


			User user = UserService.GetUserByNickname(User.Identity.Name);
			UserManageModel model = Mapper.Map<UserManageModel>(user);
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Save(UserManageModel model)
		{
			if (ModelState.IsValid)
			{
				User user = UserService.GetUserByNickname(User.Identity.Name);
				user = Mapper.Map(model, user);

				if (model.AvatarImageFile != null)
				{
					IImage avatar = ImageHelper.CreateAndSaveImageFromHttpFile(model.AvatarImageFile);
					UserService.SetAvatar(user.IdUser, avatar.IdImage);
				}

				// ChangePassword will throw an exception rather than return false in certain failure scenarios.
				bool userUpdated = UserService.UpdateUser(user);
				if (userUpdated)
				{
					return RedirectToAction("Manage");
				}
				ModelState.AddModelError("", "Profile has not been saved. Error occured.");
			}

			// If we got this far, something failed, redisplay form
			return View("Manage", model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ChangePassword(LocalPasswordModel model)
		{
			const bool hasLocalAccount = true;
			ViewBag.HasLocalPassword = hasLocalAccount;
			ViewBag.ReturnUrl = Url.Action("ChangePassword");
			if (ModelState.IsValid)
			{
				// ChangePassword will throw an exception rather than return false in certain failure scenarios.
				bool changePasswordSucceeded;
				try
				{
					User user = UserService.GetUserByNickname(User.Identity.Name);
					changePasswordSucceeded = UserService.ChangePassword(user, model.OldPassword, model.NewPassword);
				}
				catch (Exception)
				{
					changePasswordSucceeded = false;
				}

				if (changePasswordSucceeded)
				{
					return RedirectToAction("Manage", new {Message = ManageMessage.ChangePasswordSuccess});
				}
				ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
			}

			// If we got this far, something failed, redisplay form
			return View("Manage");
		}

		public enum ManageMessage
		{
			NoMessage,
			ChangePasswordSuccess,
			SetPasswordSuccess,
			RemoveLoginSuccess,
		}
	}
}