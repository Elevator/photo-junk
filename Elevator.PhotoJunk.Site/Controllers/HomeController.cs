﻿namespace Elevator.PhotoJunk.Site.Controllers
{
	using System.Web.Mvc;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Filters;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Home;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Elevator.PhotoJunk.Site.Models.User;
	using Ninject;

	[HandleExceptionFilter(View = "ErrorServer")]
	public class HomeController : Controller
	{
		[Inject]
		public IPhotoService PhotoService { get; set; }

		[Inject]
		public IUserService UserService { get; set; }

		[Inject]
		public IAlbumService AlbumService { get; set; }

		[Inject]
		public IImageService ImageService { get; set; }

		[Inject]
		public ImageHelper ImageHelper { get; set; }


		public ActionResult Index()
		{
			PagedData<User> authors = UserService.GetUsers(0, ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<Photo> photos = PhotoService.GetPhotos(0, ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<Album> albums = AlbumService.GetAlbums(0, ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);

			IndexModel model = new IndexModel
			{
				Photos = PagedMapper.Map<Photo, PhotoThumbnail>(photos),
				Albums = PagedMapper.Map<Album, AlbumThumbnail>(albums),
				Authors = PagedMapper.Map<User, UserThumbnail>(authors)
			};

			return View(model);
		}

		[HttpPost]
		public ActionResult PhotosPage(int page)
		{
			PagedData<Photo> photos = PhotoService.GetPhotos(page, ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<PhotoThumbnail> thumbnails = PagedMapper.Map<Photo, PhotoThumbnail>(photos);

			return PartialView("PhotosListPartial", thumbnails);
		}

		[HttpPost]
		public ActionResult AlbumsPage(int page)
		{
			PagedData<Album> albums = AlbumService.GetAlbums(page, ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<AlbumThumbnail> thumbnails = PagedMapper.Map<Album, AlbumThumbnail>(albums);

			return PartialView("AlbumsListPartial", thumbnails);
		}

		[HttpPost]
		public ActionResult AuthorsPage(int page)
		{
			PagedData<User> authors = UserService.GetUsers(page, ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			PagedData<UserThumbnail> thumbnails = PagedMapper.Map<User, UserThumbnail>(authors);

			return PartialView("UsersListPartial", thumbnails);
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your app description page.";

			return View();
		}
	}
}