﻿namespace Elevator.PhotoJunk.Site.Controllers
{
	using System;
	using System.Web.Mvc;
	using System.Web.Routing;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Filters;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Ninject;
	using SysImage = System.Drawing.Image;

	[HandleExceptionFilter(View = "ErrorServer")]
	public class PhotoController : Controller
	{
		//
		// GET: /Photos/
		[Inject]
		public IPhotoService PhotoService { get; set; }

		[Inject]
		public IUserService UserService { get; set; }

		[Inject]
		public IImageService ImageService { get; set; }

		[Inject]
		public IVoteService VoteService { get; set; }

		[Inject]
		public ImageHelper ImageHelper { get; set; }

		[HttpGet]
		public ActionResult Index()
		{
			PagedData<PhotoThumbnail> thumbnails = GetIndexPage("", 0);
			return View(thumbnails);
		}

		[HttpPost]
		public ActionResult IndexPage(string keyWords, int page)
		{
			PagedData<PhotoThumbnail> thumbnails = GetIndexPage(keyWords, page);
			return PartialView("PhotosListPartial", thumbnails);
		}

		private PagedData<PhotoThumbnail> GetIndexPage(string keyWords, int page)
		{
			int numberOfThumbnails = ConfigHelper.NumberOfThumbnailsForSpecializedPage;

			PagedData<Photo> photos = string.IsNullOrEmpty(keyWords)
				? PhotoService.GetPhotos(page, numberOfThumbnails)
				: PhotoService.FilterPhotosByKeywords(keyWords, page, numberOfThumbnails);

			return PagedMapper.Map<Photo, PhotoThumbnail>(photos);
		}

		[HttpGet]
		[Authorize]
		public ActionResult MyPhotos()
		{
			PagedData<PhotoThumbnail> thumbnails = GetPhotosByUserPage(User.Identity.Name, 0);
			return View(thumbnails);
		}

		[HttpGet]
		public ActionResult PhotosByUser(string userName)
		{
			PagedData<PhotoThumbnail> thumbnails = GetPhotosByUserPage(userName, 0);

			UserPhotosModel model = new UserPhotosModel
			{
				UserName = userName,
				Photos = thumbnails
			};
			return View(model);
		}

		[HttpPost]
		public ActionResult PhotosByUserPage(string userName, int page)
		{
			PagedData<PhotoThumbnail> thumbnails = GetPhotosByUserPage(userName, page);
			return PartialView("PhotosAnonymousListPartial", thumbnails);
		}

		private PagedData<PhotoThumbnail> GetPhotosByUserPage(string userName, int page)
		{
			User author = UserService.GetUserByNickname(userName);
			PagedData<Photo> photos = PhotoService.FilterPhotosByAuthor(author.IdUser, page,
				ConfigHelper.NumberOfThumbnailsForSpecializedPage);

			return PagedMapper.Map<Photo, PhotoThumbnail>(photos);
		}

		[HttpGet]
		public ActionResult Search()
		{
			PhotoSearchModel model = new PhotoSearchModel();
			return View(model);
		}

		[HttpPost]
		public ActionResult SearchPage(PhotoSearchModel model, int page)
		{
			PagedData<Photo> photos = PhotoService.FilterPhotos(model.Title, model.Descriprion, model.Place, model.Camera,
				model.AuthorName, page, ConfigHelper.NumberOfThumbnailsForSpecializedPage);

			PagedData<PhotoThumbnail> thumbnails = PagedMapper.Map<Photo, PhotoThumbnail>(photos);
			return PartialView("PhotosListPartial", thumbnails);
		}

		[HttpGet]
		public ActionResult Show(Guid id)
		{
			Photo photo = PhotoService.GetPhoto(id);
			PhotoShowModel model = Mapper.Map<PhotoShowModel>(photo);

			if (Request.IsAuthenticated)
			{
				User user = UserService.GetUserByNickname(User.Identity.Name);
				model.CanVote = VoteService.UserCanVote(user.IdUser, id);
			}

			return View(model);
		}

		[Authorize]
		public ActionResult Like(Guid id)
		{
			return Vote(id, Logic.Services.Abstract.Vote.Like);
		}

		[Authorize]
		public ActionResult Dislike(Guid id)
		{
			return Vote(id, Logic.Services.Abstract.Vote.Dislike);
		}

		private ActionResult Vote(Guid photoId, Vote vote)
		{
			User user = UserService.GetUserByNickname(User.Identity.Name);
			bool canVote = VoteService.UserCanVote(user.IdUser, photoId);

			if (canVote)
				VoteService.VotePhoto(user.IdUser, photoId, vote);

			if (Request.IsAjaxRequest())
			{
				int currentLikes = VoteService.GetLikesForPhoto(photoId);
				int currentDislikes = VoteService.GetDislikesForPhoto(photoId);
				return Json(new {success = canVote, likes = currentLikes, dislikes = currentDislikes});
			}
			return Show(photoId);
		}

		[HttpGet]
		public ActionResult Edit(Guid id)
		{
			Photo photo = PhotoService.GetPhoto(id);
			PhotoEditModel model = Mapper.Map<PhotoEditModel>(photo);
			return View(model);
		}

		[HttpGet]
		public ActionResult Delete(Guid id)
		{
			PhotoService.DeletePhoto(id);
			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult Add()
		{
			return View("Edit", CreatePhotoEditModel());
		}

		[HttpPost]
		public ActionResult Save(PhotoEditModel model)
		{
			User author = UserService.GetUserById(model.AuthorId);
			if (!UserCanCreatePhoto(author))
			{
				ModelState.AddModelError("",
					string.Format("You are not allowed to create more than {0} photos. Get a premium account to ignore this limit.",
						ConfigHelper.MaxPhotosForNonPremium));
			}

			if (ModelState.IsValid)
			{
				if (model.IsNew)
				{
					Photo photo = Mapper.Map<Photo>(model);
					model.Id = PhotoService.SavePhoto(photo).IdPhoto;
				}
				else
				{
					Photo photo = PhotoService.GetPhoto(model.Id);
					photo = Mapper.Map(model, photo);
					PhotoService.UpdatePhoto(photo);
				}
				return RedirectToAction("Show", new RouteValueDictionary {{"id", model.Id}});
			}
			return View("Edit", model);
		}

		private bool UserCanCreatePhoto(User user)
		{
			PagedData<Photo> photos = PhotoService.FilterPhotosByAuthor(user.IdUser, 0, 1);
			return user.IsPremium || photos.TotalItems < ConfigHelper.MaxPhotosForNonPremium;
		}

		private PhotoEditModel CreatePhotoEditModel()
		{
			string name = User.Identity.Name;
			User user = UserService.GetUserByNickname(name);
			return new PhotoEditModel
			{
				Id = Guid.Empty,
				Title = null,
				Place = null,
				Description = null,
				Camera = null,
				FocalLength = 0,
				ApertureSize = 0,
				IsoSpeed = 0,
				Flash = false,
				AuthorId = user.IdUser,
				ImageFileName = null,
				FullImageFileName = null,
				IsNew = true
			};
		}
	}
}