﻿namespace Elevator.PhotoJunk.Site.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;
	using System.Web.Routing;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Filters;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Ninject;
	using SysImage = System.Drawing.Image;

	[HandleExceptionFilter(View = "ErrorServer")]
	public class AlbumController : Controller
	{
		[Inject]
		public IAlbumService AlbumService { get; set; }

		[Inject]
		public IPhotoService PhotoService { get; set; }

		[Inject]
		public IUserService UserService { get; set; }

		[Inject]
		public IImageService ImageService { get; set; }

		[Inject]
		public ImageHelper ImageHelper { get; set; }

		[HttpGet]
		public ActionResult Index()
		{
			PagedData<AlbumThumbnail> thumbnails = GetIndexPage("", 0);
			return View(thumbnails);
		}

		[HttpPost]
		public ActionResult IndexPage(string keyWords, int page)
		{
			PagedData<AlbumThumbnail> thumbnails = GetIndexPage(keyWords, page);
			return PartialView("AlbumsListPartial", thumbnails);
		}

		private PagedData<AlbumThumbnail> GetIndexPage(string keyWords, int page)
		{
			int numberOfThumbnails = ConfigHelper.NumberOfThumbnailsForSpecializedPage;

			PagedData<Album> albums = string.IsNullOrEmpty(keyWords)
				? AlbumService.GetAlbums(page, numberOfThumbnails)
				: AlbumService.FilterAlbumsByKeywords(keyWords, page, numberOfThumbnails);

			return PagedMapper.Map<Album, AlbumThumbnail>(albums);
		}

		[HttpGet]
		[Authorize]
		public ActionResult MyAlbums()
		{
			PagedData<AlbumThumbnail> thumbnails = GetAlbumsByUserPage(User.Identity.Name, 0);
			return View(thumbnails);
		}

		[HttpGet]
		public ActionResult AlbumsByUser(string userName)
		{
			PagedData<AlbumThumbnail> thumbnails = GetAlbumsByUserPage(userName, 0);

			UserAlbumsModel model = new UserAlbumsModel
			{
				UserName = userName,
				Albums = thumbnails
			};
			return View(model);
		}

		[HttpPost]
		public ActionResult AlbumsByUserPage(string userName, int page)
		{
			PagedData<AlbumThumbnail> thumbnails = GetAlbumsByUserPage(userName, page);
			return PartialView("AlbumsAnonymousListPartial", thumbnails);
		}

		private PagedData<AlbumThumbnail> GetAlbumsByUserPage(string userName, int page)
		{
			User author = UserService.GetUserByNickname(userName);
			PagedData<Album> albums = AlbumService.FilterAlbumsByAuthor(author.IdUser, page,
				ConfigHelper.NumberOfThumbnailsForSpecializedPage);

			return PagedMapper.Map<Album, AlbumThumbnail>(albums);
		}

		[HttpGet]
		public ActionResult Search()
		{
			AlbumSearchModel model = new AlbumSearchModel();
			return View(model);
		}

		[HttpPost]
		public ActionResult SearchPage(AlbumSearchModel model, int page)
		{
			PagedData<Album> albums = AlbumService.FilterAlbums(model.Title, model.Descriprion, model.AuthorName, page,
				ConfigHelper.NumberOfThumbnailsForSpecializedPage);

			PagedData<AlbumThumbnail> thumbnails = PagedMapper.Map<Album, AlbumThumbnail>(albums);
			return PartialView("AlbumsListPartial", thumbnails);
		}

		[HttpGet]
		public ActionResult Show(Guid id)
		{
			Album album = AlbumService.GetAlbumById(id);
			AlbumShowModel model = Mapper.Map<AlbumShowModel>(album);
			return View(model);
		}

		[HttpGet]
		public ActionResult ShowByName(string name)
		{
			Album album = AlbumService.GetAlbumByTitle(name);
			AlbumShowModel model = Mapper.Map<AlbumShowModel>(album);
			return View("Show", model);
		}

		[HttpPost]
		public ActionResult PhotosInAlbumPage(string albumName, int page)
		{
			Album album = AlbumService.GetAlbumByTitle(albumName);
			PagedData<Photo> photos = PhotoService.FilterPhotosByAlbum(album.IdAlbum, page,
				ConfigHelper.NumberOfThumbnailsForSpecializedPage);
			PagedData<PhotoThumbnail> thumbnails = PagedMapper.Map<Photo, PhotoThumbnail>(photos);

			return PartialView("PhotosAnonymousListPartial", thumbnails);
		}

		[HttpGet]
		public ActionResult Edit(Guid id)
		{
			Album album = AlbumService.GetAlbumById(id);
			AlbumEditModel model = Mapper.Map<AlbumEditModel>(album);
			return View(model);
		}

		[HttpGet]
		public ActionResult Create()
		{
			return View("Edit", CreateAlbumEditModel());
		}

		[HttpGet]
		public ActionResult Manage(Guid id)
		{
			Album album = AlbumService.GetAlbumById(id);
			User author = UserService.GetUserByNickname(User.Identity.Name);

			HashSet<Guid> photosInAlbum =
				new HashSet<Guid>(PhotoService.FilterPhotosByAlbum(id, 0, int.MaxValue).Select(p => p.IdPhoto));
			IEnumerable<Photo> photosOfUser = PhotoService.FilterPhotosByAuthor(author.IdUser, 0, int.MaxValue);

			List<PhotoCheck> photoChecks = photosOfUser.Select(photo => new PhotoCheck
			{
				Checked = photosInAlbum.Contains(photo.IdPhoto),
				Photo = Mapper.Map<PhotoThumbnail>(photo)
			}).ToList();

			AlbumManageModel albumManageModel = new AlbumManageModel
			{
				Album = Mapper.Map<AlbumThumbnail>(album),
				Photos = photoChecks
			};

			return View(albumManageModel);
		}

		[HttpPost]
		public ActionResult Manage(AlbumManageModel model)
		{
			Album album = AlbumService.GetAlbumById(model.Album.AlbumId);

			List<Guid> photosInAlbum =
				PhotoService.FilterPhotosByAlbum(album.IdAlbum, 0, int.MaxValue).Select(p => p.IdPhoto).ToList();
			List<Guid> checkedPhotos = model.Photos.Where(p => p.Checked).Select(p => p.Photo.PhotoId).ToList();

			List<Guid> photosToAdd = checkedPhotos.Where(id => !photosInAlbum.Contains(id)).ToList();
			List<Guid> photosToRemove = photosInAlbum.Where(id => !checkedPhotos.Contains(id)).ToList();

			foreach (Guid photoId in photosToAdd)
				PhotoService.AddPhotoToAlbum(photoId, album.IdAlbum);

			foreach (Guid photoId in photosToRemove)
				PhotoService.RemovePhotoFromAlbum(photoId, album.IdAlbum);

			return RedirectToAction("Show", new RouteValueDictionary {{"id", album.IdAlbum}});
		}

		[HttpGet]
		public ActionResult Delete(Guid id)
		{
			AlbumService.DeleteAlbum(id);
			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult Save(AlbumEditModel model)
		{
			User author = UserService.GetUserById(model.AuthorId);
			if (!UserCanCreateAlbum(author))
			{
				ModelState.AddModelError("",
					string.Format("You are not allowed to create more than {0} albums. Get a premium account to ignore this limit.", ConfigHelper.MaxAlbumsForNonPremium));
			}

			if (ModelState.IsValid)
			{
				if (model.IsNew)
				{
					Album album = Mapper.Map<Album>(model);
					model.Id = AlbumService.SaveAlbum(album).IdAlbum;
				}
				else
				{
					Album album = AlbumService.GetAlbumById(model.Id);
					album = Mapper.Map(model, album);
					AlbumService.UpdateAlbum(album);
				}
				return RedirectToAction("Show", new RouteValueDictionary {{"id", model.Id}});
			}
			return View("Edit", model);
		}

		private bool UserCanCreateAlbum(User user)
		{
			PagedData<Album> albums = AlbumService.FilterAlbumsByAuthor(user.IdUser, 0, 1);
			return user.IsPremium || albums.TotalItems < ConfigHelper.MaxAlbumsForNonPremium;
		}

		private AlbumEditModel CreateAlbumEditModel()
		{
			string name = User.Identity.Name;
			User user = UserService.GetUserByNickname(name);
			return new AlbumEditModel
			{
				IsNew = true,
				Id = Guid.Empty,
				AuthorId = user.IdUser,
				Title = null,
				Description = null,
				CoverImageFileName = null
			};
		}
	}
}