﻿namespace Elevator.PhotoJunk.Site
{
	using System.Web.Mvc;
	using System.Web.Routing;

	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute("ShortAlbumRef", "ShowAlbum/{name}", new { controller = "Album", action = "ShowByName" });
			routes.MapRoute("ShortPhotoRef", "ShowPhoto/{id}", new { controller = "Photo", action = "Show" });
			routes.MapRoute("ShortAuthorRef", "ShowAuthor/{name}", new { controller = "User", action = "Show" });

			routes.MapRoute("Albums", "Albums", new { controller = "Album", action = "Index" });
			routes.MapRoute("Photos", "Photos", new { controller = "Photo", action = "Index" });
			routes.MapRoute("Users", "Users", new { controller = "User", action = "Index" });

			routes.MapRoute("MyAlbums", "MyAlbums", new { controller = "Album", action = "MyAlbums" });
			routes.MapRoute("MyPhotos", "MyPhotos", new { controller = "Photo", action = "MyPhotos" });

			routes.MapRoute("Default", "{controller}/{action}/{id}",new { controller = "Home", action = "Index", id = "" });

		}
	}
}