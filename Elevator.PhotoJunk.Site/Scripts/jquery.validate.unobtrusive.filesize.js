﻿$(function () {
	jQuery.validator.unobtrusive.adapters.add('filesize', ['maxsize'], function (options) {
		var rules = {
			maxSize: options.params.maxsize
		};

		options.rules['filesize'] = rules;
		if (options.message) {
			options.messages['filesize'] = options.message;
		}
	});

	jQuery.validator.addMethod("filesize", function (value, element, param) {
		var size = element.files[0].size;
		var sizeIsValid = size <= param.maxSize * 1024;
		return sizeIsValid;
	});

}(jQuery));
