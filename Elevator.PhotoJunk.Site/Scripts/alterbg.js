﻿(function ($) {
	var colorAlterElems;
	var colorAlterOuterElems;
	var alterColorTriggers;

	var saturation = 20;
	var value = 60;
	var hue = 0;

	function hsv2Rgb(h, s, v) {
		h /= 360;
		s /= 100;
		v /= 100;

		var m2 = v <= 0.5 ? v * (s + 1) : v + s - v * s;
		var m1 = v * 2 - m2;

		var r = norm2Hex(hue2Rgb(m1, m2, h + 1 / 3));
		var g = norm2Hex(hue2Rgb(m1, m2, h));
		var b = norm2Hex(hue2Rgb(m1, m2, h - 1 / 3));

		return r + '' + g + '' + b;
	}

	function hue2Rgb(m1, m2, h) {
		if (h < 0) h = h + 1;
		if (h > 1) h = h - 1;

		if (h * 6 < 1) return m1 + (m2 - m1) * h * 6;
		if (h * 2 < 1) return m2;
		if (h * 3 < 2) return m1 + (m2 - m1) * (2 / 3 - h) * 6;

		return m1;
	}

	function norm2Hex(value) {
		return dec2Hex(Math.floor(255 * value));
	}

	function dec2Hex(dec) {
		var hexChars = "0123456789ABCDEF";
		var a = dec % 16;
		var b = (dec - a) / 16;

		var hex = '' + hexChars.charAt(b) + hexChars.charAt(a);

		return hex;
	}

	function setLogoColor(h, s, v) {
		colorAlterElems.css('background-color', '#' + hsv2Rgb(h, s, v));
		colorAlterOuterElems.css('background-color', '#' + hsv2Rgb(h, s, v/3));
	}

	function alterColor() {
		hue = Math.floor(360 * Math.random());
		setLogoColor(hue, saturation, value);
	}

	function initAlterColors() {
		colorAlterElems = $('.alter_bg');
		colorAlterOuterElems = $('.alter_outer_bg');

		alterColorTriggers = $('.alter_bg_trigger');
		alterColorTriggers.mouseover(alterColor);
	}

	$(function () {
		initAlterColors();
		setLogoColor(hue, saturation, value);
	});
})(jQuery);