﻿$(function () {
	jQuery.validator.unobtrusive.adapters.add('filemimetype', ['validmimetypes'], function (options) {
		var params = {
			validmimetypes: options.params.validmimetypes.split(',')
		};

		options.rules['filemimetype'] = params;
		if (options.message) {
			options.messages['filemimetype'] = options.message;
		}
	});

	jQuery.validator.addMethod("filemimetype", function (value, element, param) {
		var type = element.files[0].type;
		var typeIsValid = $.inArray(type, param.validmimetypes) !== -1;
		return typeIsValid;
	});

}(jQuery));
