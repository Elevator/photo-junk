﻿namespace Elevator.PhotoJunk.Site.Mapping.Profiles
{
	using System;
	using System.Web;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Microsoft.Practices.ServiceLocation;

	public class PhotoProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<Photo, PhotoEditModel>()
				.ForMember(x => x.IsNew, m => m.UseValue(false))
				.ForMember(x => x.Id, m => m.MapFrom(s => s.IdPhoto))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Place, m => m.MapFrom(s => s.Place))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.Camera, m => m.MapFrom(s => s.Camera))
				.ForMember(x => x.FocalLength, m => m.MapFrom(s => s.FocalLength))
				.ForMember(x => x.ApertureSize, m => m.MapFrom(s => s.ApertureSize))
				.ForMember(x => x.IsoSpeed, m => m.MapFrom(s => s.IsoSpeed))
				.ForMember(x => x.Flash, m => m.MapFrom(s => s.Flash))
				.ForMember(x => x.AuthorId, m => m.MapFrom(s => s.Author.IdUser))
				.ForMember(x => x.ImageFileName,
					m => m.ResolveUsing(s => GetResizedImageFileName(s.Image, ConfigHelper.PreviewHeightPx)))
				.ForMember(x => x.FullImageFile, m => m.Ignore())
				.ForMember(x => x.FullImageFileName, m => m.ResolveUsing(s => GetFullImageFileName(s.Image)));

			Mapper.CreateMap<PhotoEditModel, Photo>()
				.ConstructUsing(source => new Photo(
					source.Id,
					source.Title,
					source.Description,
					source.Place,
					source.Camera,
					source.FocalLength,
					source.ApertureSize,
					source.IsoSpeed,
					source.Flash,
					GetUser(source.AuthorId),
					CreateAndSaveImageFromHttpFile(source.FullImageFile),
					DateTime.Now))
				.ForMember(x => x.IdPhoto, m => m.MapFrom(s => s.Id))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.Place, m => m.MapFrom(s => s.Place))
				.ForMember(x => x.Camera, m => m.MapFrom(s => s.Camera))
				.ForMember(x => x.FocalLength, m => m.MapFrom(s => s.FocalLength))
				.ForMember(x => x.ApertureSize, m => m.MapFrom(s => s.ApertureSize))
				.ForMember(x => x.IsoSpeed, m => m.MapFrom(s => s.IsoSpeed))
				.ForMember(x => x.Flash, m => m.MapFrom(s => s.Flash))
				.ForMember(x => x.Author, m => m.Ignore()) // We cannot change the author of photo
				.ForMember(x => x.UploadDate, m => m.Ignore())
				.ForMember(x => x.Image, m => m.Ignore()) // we want to map this property only if x.CoverImageFile is not null
				.AfterMap((s, d) => { if (s.FullImageFile != null) d.Image = CreateAndSaveImageFromHttpFile(s.FullImageFile); });

			Mapper.CreateMap<Photo, PhotoShowModel>()
				.ForMember(x => x.Id, m => m.MapFrom(s => s.IdPhoto))
				.ForMember(x => x.AuthorName, m => m.MapFrom(s => s.Author.Nickname))
				.ForMember(x => x.AuthorEmail, m => m.MapFrom(s => s.Author.Email))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Place, m => m.MapFrom(s => s.Place))
				.ForMember(x => x.UploadDate, m => m.MapFrom(s => s.UploadDate))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.Camera, m => m.MapFrom(s => s.Camera))
				.ForMember(x => x.FocalLength, m => m.MapFrom(s => s.FocalLength))
				.ForMember(x => x.ApertureSize, m => m.MapFrom(s => s.ApertureSize))
				.ForMember(x => x.IsoSpeed, m => m.MapFrom(s => s.IsoSpeed))
				.ForMember(x => x.Flash, m => m.MapFrom(s => s.Flash))
				.ForMember(x => x.Likes, m => m.ResolveUsing(s => GetLikes(s.IdPhoto)))
				.ForMember(x => x.Dislikes, m => m.ResolveUsing(s => GetDislikes(s.IdPhoto)))
				.ForMember(x => x.CanVote, m => m.UseValue(true))
				.ForMember(x => x.ImageFileName,
					m => m.ResolveUsing(s => GetResizedImageFileName(s.Image, ConfigHelper.PreviewHeightPx)))
				.ForMember(x => x.FullImageFileName, m => m.ResolveUsing(s => GetFullImageFileName(s.Image)));

			Mapper.CreateMap<Photo, PhotoThumbnail>()
				.ForMember(x => x.PhotoId, m => m.MapFrom(s => s.IdPhoto))
				.ForMember(x => x.AuthorName, m => m.MapFrom(s => s.Author.Nickname))
				.ForMember(x => x.AuthorEmail, m => m.MapFrom(s => s.Author.Email))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.ImageFileName,
					m => m.ResolveUsing(s => GetResizedImageFileName(s.Image, ConfigHelper.ThumbnailHeightPx)));
		}

		private static string GetFullImageFileName(IImage image)
		{
			ImageHelper imageHelper = ServiceLocator.Current.GetInstance<ImageHelper>();
			return imageHelper.GetImageUrl(imageHelper.GetImageFilePath(image));
		}

		private static string GetResizedImageFileName(IImage image, int height)
		{
			ImageHelper imageHelper = ServiceLocator.Current.GetInstance<ImageHelper>();
			return imageHelper.GetImageUrl(imageHelper.GetResizedImageFilePath(image, height));
		}

		private static int GetLikes(Guid photoId)
		{
			IVoteService voteService = ServiceLocator.Current.GetInstance<IVoteService>();
			return voteService.GetLikesForPhoto(photoId);
		}

		private static int GetDislikes(Guid photoId)
		{
			IVoteService voteService = ServiceLocator.Current.GetInstance<IVoteService>();
			return voteService.GetDislikesForPhoto(photoId);
		}

		private static IUser GetUser(Guid userId)
		{
			return ServiceLocator.Current.GetInstance<IUserService>().GetUserById(userId);
		}

		private static IImage CreateAndSaveImageFromHttpFile(HttpPostedFileBase file)
		{
			return ServiceLocator.Current.GetInstance<ImageHelper>().CreateAndSaveImageFromHttpFile(file);
		}
	}
}