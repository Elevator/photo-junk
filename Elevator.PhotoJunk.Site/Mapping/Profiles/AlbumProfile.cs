﻿namespace Elevator.PhotoJunk.Site.Mapping.Profiles
{
	using System;
	using System.Web;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Microsoft.Practices.ServiceLocation;

	public class AlbumProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<Album, AlbumEditModel>()
				.ForMember(x => x.IsNew, m => m.UseValue(false))
				.ForMember(x => x.Id, m => m.MapFrom(s => s.IdAlbum))
				.ForMember(x => x.AuthorId, m => m.MapFrom(s => s.Author.IdUser))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.IdAlbum))
				.ForMember(x => x.CoverImageFile, m => m.Ignore())
				.ForMember(x => x.CoverImageFileName,
					m => m.ResolveUsing(s => GetImageFileName(s.Cover, ConfigHelper.ThumbnailHeightPx)));

			Mapper.CreateMap<AlbumEditModel, Album>()
				.ConstructUsing(source => new Album(
					source.Id,
					GetUser(source.AuthorId),
					source.Title,
					source.Description,
					CreateAndSaveImageFromHttpFile(source.CoverImageFile),
					DateTime.Now))
				.ForMember(x => x.IdAlbum, m => m.Ignore())
				.ForMember(x => x.Author, m => m.ResolveUsing(s => GetUser(s.AuthorId)))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.UploadDate, m => m.Ignore())
				.ForMember(x => x.Cover, m => m.Ignore()) // we want to map this property only if x.CoverImageFile is not null
				.AfterMap((s, d) => { if (s.CoverImageFile != null) d.Cover = CreateAndSaveImageFromHttpFile(s.CoverImageFile); });

			Mapper.CreateMap<Album, AlbumShowModel>()
				.ForMember(x => x.Id, m => m.MapFrom(s => s.IdAlbum))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.AuthorName, m => m.MapFrom(s => s.Author.Nickname))
				.ForMember(x => x.AuthorEmail, m => m.MapFrom(s => s.Author.Email))
				.ForMember(x => x.AuthorId, m => m.MapFrom(s => s.Author.IdUser))
				.ForMember(x => x.UploadDate, m => m.MapFrom(s => s.UploadDate))
				.ForMember(x => x.CoverImageFileName,
					m => m.ResolveUsing(s => GetImageFileName(s.Cover, ConfigHelper.PreviewHeightPx)))
				.ForMember(x => x.Photos, m => m.ResolveUsing(s => GetAlbumPhotos(s.IdAlbum)));

			Mapper.CreateMap<Album, AlbumThumbnail>()
				.ForMember(x => x.AlbumId, m => m.MapFrom(s => s.IdAlbum))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.AuthorName, m => m.MapFrom(s => s.Author.Nickname))
				.ForMember(x => x.AuthorEmail, m => m.MapFrom(s => s.Author.Email))
				.ForMember(x => x.CoverFileName, m => m.ResolveUsing(s => GetImageFileName(s.Cover, ConfigHelper.ThumbnailHeightPx)));
		}

		private static string GetImageFileName(IImage image, int height)
		{
			ImageHelper imageHelper = ServiceLocator.Current.GetInstance<ImageHelper>();
			return imageHelper.GetImageUrl(imageHelper.GetResizedImageFilePath(image, height));
		}

		private static IUser GetUser(Guid userId)
		{
			return ServiceLocator.Current.GetInstance<IUserService>().GetUserById(userId);
		}

		private static IImage CreateAndSaveImageFromHttpFile(HttpPostedFileBase file)
		{
			return ServiceLocator.Current.GetInstance<ImageHelper>().CreateAndSaveImageFromHttpFile(file);
		}

		private static PagedData<PhotoThumbnail> GetAlbumPhotos(Guid albumId)
		{
			IPhotoService photoService = ServiceLocator.Current.GetInstance<IPhotoService>();
			PagedData<Photo> photos = photoService.FilterPhotosByAlbum(albumId, 0, ConfigHelper.NumberOfThumbnailsForSpecializedPage);
			return PagedMapper.Map<Photo, PhotoThumbnail>(photos);
		}
	}
}