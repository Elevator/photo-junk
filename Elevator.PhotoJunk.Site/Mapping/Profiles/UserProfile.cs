﻿namespace Elevator.PhotoJunk.Site.Mapping.Profiles
{
	using System;
	using AutoMapper;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Elevator.PhotoJunk.Site.Models.User;
	using Microsoft.Practices.ServiceLocation;

	public class UserProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<User, UserManageModel>()
				.ForMember(x => x.Nickname, m => m.MapFrom(s => s.Nickname))
				.ForMember(x => x.FirstName, m => m.MapFrom(s => s.FirstName))
				.ForMember(x => x.LastName, m => m.MapFrom(s => s.LastName))
				.ForMember(x => x.MiddleName, m => m.MapFrom(s => s.MiddleName))
				.ForMember(x => x.Location, m => m.MapFrom(s => s.Location))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.IsPremium, m => m.MapFrom(s => s.IsPremium))
				.ForMember(x => x.AvatarImageFile, m => m.Ignore())
				.ForMember(x => x.AvatarImageFileName,
					m => m.ResolveUsing(s => GetAvatarFileName(s.IdUser, ConfigHelper.ThumbnailHeightPx)));

			Mapper.CreateMap<UserManageModel, User>()
				.ForMember(x => x.IdUser, m => m.Ignore())
				.ForMember(x => x.Email, m => m.Ignore())
				.ForMember(x => x.Password, m => m.Ignore())
				.ForMember(x => x.Nickname, m => m.MapFrom(s => s.Nickname))
				.ForMember(x => x.FirstName, m => m.MapFrom(s => s.FirstName))
				.ForMember(x => x.LastName, m => m.MapFrom(s => s.LastName))
				.ForMember(x => x.MiddleName, m => m.MapFrom(s => s.MiddleName))
				.ForMember(x => x.Location, m => m.MapFrom(s => s.Location))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.RegistrationDate, m => m.Ignore())
				.ForMember(x => x.IsPremium, m => m.MapFrom(s => s.IsPremium));

			Mapper.CreateMap<User, UserShowModel>()
				.ForMember(x => x.Id, m => m.MapFrom(s => s.IdUser))
				.ForMember(x => x.Nickname, m => m.MapFrom(s => s.Nickname))
				.ForMember(x => x.FirstName, m => m.MapFrom(s => s.FirstName))
				.ForMember(x => x.LastName, m => m.MapFrom(s => s.LastName))
				.ForMember(x => x.MiddleName, m => m.MapFrom(s => s.MiddleName))
				.ForMember(x => x.Location, m => m.MapFrom(s => s.Location))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.RegistrationDate, m => m.MapFrom(s => s.RegistrationDate))
				.ForMember(x => x.IsPremium, m => m.MapFrom(s => s.IsPremium))
				.ForMember(x => x.AvatarImageFileName,
					m => m.ResolveUsing(s => GetAvatarFileName(s.IdUser, ConfigHelper.PreviewHeightPx)))
				.ForMember(x => x.Albums, m => m.ResolveUsing(s => GetAlbums(s.IdUser)))
				.ForMember(x => x.Photos, m => m.ResolveUsing(s => GetPhotos(s.IdUser)));

			Mapper.CreateMap<User, UserThumbnail>()
				.ForMember(x => x.UserId, m => m.MapFrom(s => s.IdUser))
				.ForMember(x => x.Nickname, m => m.MapFrom(s => s.Nickname))
				.ForMember(x => x.FirstName, m => m.MapFrom(s => s.FirstName))
				.ForMember(x => x.LastName, m => m.MapFrom(s => s.LastName))
				.ForMember(x => x.MiddleName, m => m.MapFrom(s => s.MiddleName))
				.ForMember(x => x.Location, m => m.MapFrom(s => s.Location))
				.ForMember(x => x.AvatarImageFileName,
					m => m.ResolveUsing(s => GetAvatarFileName(s.IdUser, ConfigHelper.ThumbnailHeightPx)));
		}

		private static PagedData<PhotoThumbnail> GetPhotos(Guid userId)
		{
			IPhotoService photoService = ServiceLocator.Current.GetInstance<IPhotoService>();
			PagedData<Photo> photos = photoService.FilterPhotosByAuthor(userId, 0,
				ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			return PagedMapper.Map<Photo, PhotoThumbnail>(photos);
		}

		private static PagedData<AlbumThumbnail> GetAlbums(Guid userId)
		{
			IAlbumService albumService = ServiceLocator.Current.GetInstance<IAlbumService>();
			PagedData<Album> albums = albumService.FilterAlbumsByAuthor(userId, 0,
				ConfigHelper.NumberOfThumbnailsForNonSpecializedPage);
			return PagedMapper.Map<Album, AlbumThumbnail>(albums);
		}

		private string GetAvatarFileName(Guid userId, int height)
		{
			IUserService userService = ServiceLocator.Current.GetInstance<IUserService>();
			IImageService imageService = ServiceLocator.Current.GetInstance<IImageService>();
			ImageHelper imageHelper = ServiceLocator.Current.GetInstance<ImageHelper>();

			string avatarImageFileName =
				userService.UserHasAvatar(userId)
					? imageHelper.GetImageUrl(
						imageHelper.GetResizedImageFilePath(imageService.GetImage(userService.GetAvatarImageId(userId)), height))
					: ConfigHelper.NoAvatarImageUrl;
			return avatarImageFileName;
		}
	}
}