﻿namespace Elevator.PhotoJunk.Site.Mapping
{
	using AutoMapper;
	using Elevator.PhotoJunk.Site.Mapping.Profiles;

	public static class AutomapperSiteConfiguration
	{
		public static void Configure(IConfiguration configuration)
		{
			configuration.AddProfile(new AlbumProfile());
			configuration.AddProfile(new PhotoProfile());
			configuration.AddProfile(new UserProfile());
		}
	}
}