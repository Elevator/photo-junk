﻿namespace Elevator.PhotoJunk.Site.Validators
{
	using System.ComponentModel.DataAnnotations;
	using Elevator.PhotoJunk.Site.Models.Album;

	public class AlbumEditModelAttribute : ValidationAttribute
	{
		private const string _notAnAlbumErrorMessage = "It is not a album! How have you made it?";
		private const string _noCoverFileErrorMessage = "Please upload a cover for this album";

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value == null)
				return ValidationResult.Success;

			AlbumEditModel albumEditModel = value as AlbumEditModel;
			if (albumEditModel == null)
				return new ValidationResult(_notAnAlbumErrorMessage);

			if (albumEditModel.IsNew && albumEditModel.CoverImageFile == null)
				return new ValidationResult(_noCoverFileErrorMessage);

			return ValidationResult.Success;
		}
	}
}