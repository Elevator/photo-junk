﻿namespace Elevator.PhotoJunk.Site.Validators
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.Globalization;
	using System.Web;
	using System.Web.Configuration;
	using System.Web.Mvc;

	[AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
	public class FileSizeAttribute : ValidationAttribute, IClientValidatable
	{
		private readonly int _maxFileSizeKb;

		public FileSizeAttribute(int maxFileSizeKb)
		{
			_maxFileSizeKb = maxFileSizeKb;
		}

		public FileSizeAttribute(string settingsKey)
		{
			_maxFileSizeKb = int.Parse(WebConfigurationManager.AppSettings[settingsKey]);
		}

		private string FormatExtensionErrorMessage(string displayName)
		{
			return string.Format(
					CultureInfo.CurrentUICulture,
					"File size limit ({0} KB) is exceeded for {1}",
					_maxFileSizeKb, displayName);
		}

		private string FormatNotAFileErrorMessage(string displayName)
		{
			return string.Format(CultureInfo.CurrentUICulture, "{0} should be a file", displayName);
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value == null)
				return ValidationResult.Success;

			HttpPostedFileBase file = value as HttpPostedFileBase;
			if (file == null)
				return new ValidationResult(FormatNotAFileErrorMessage(validationContext.DisplayName));

			int fileSizeKb = (int) (file.InputStream.Length/1024);
			if (fileSizeKb > _maxFileSizeKb)
				return new ValidationResult(FormatExtensionErrorMessage(validationContext.DisplayName));

			return ValidationResult.Success;
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
			ControllerContext context)
		{
			yield return CreateFileSizeValidationRule(FormatExtensionErrorMessage(metadata.GetDisplayName()), _maxFileSizeKb);
		}

		private ModelClientValidationRule CreateFileSizeValidationRule(string errorMessage, int maxFileSizeKb)
		{
			ModelClientValidationRule rule = new ModelClientValidationRule
			{
				ErrorMessage = errorMessage,
				ValidationType = "filesize",
			};
			rule.ValidationParameters.Add("maxsize", maxFileSizeKb);

			return rule;
		}

	}
}