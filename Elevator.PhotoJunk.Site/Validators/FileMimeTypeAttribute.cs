﻿namespace Elevator.PhotoJunk.Site.Validators
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.Globalization;
	using System.Linq;
	using System.Web;
	using System.Web.Configuration;
	using System.Web.Mvc;
	using Elevator.PhotoJunk.Site.Helpers;

	[AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
	public class FileMimeTypeAttribute : ValidationAttribute, IClientValidatable
	{
		private readonly IEnumerable<string> _validMimeTypes;

		public FileMimeTypeAttribute(IEnumerable<string> validMimeTypes)
		{
			_validMimeTypes = validMimeTypes;
		}

		public FileMimeTypeAttribute(string settingsKey)
		{
			_validMimeTypes = WebConfigurationManager.AppSettings[settingsKey].Split(new[] {' ', ','}, StringSplitOptions.RemoveEmptyEntries);
		}

		private string FormatMimeTypeErrorMessage(string displayName)
		{
			return string.Format(
				CultureInfo.CurrentUICulture,
				"This file type is not supported for {0}. Possible types are: {1}",
				displayName, string.Join(", ", _validMimeTypes));
		}

		private string FormatNotAFileErrorMessage(string displayName)
		{
			return string.Format(CultureInfo.CurrentUICulture, "{0} should be a file", displayName);
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value == null)
				return ValidationResult.Success;

			HttpPostedFileBase file = value as HttpPostedFileBase;
			if (file == null)
				return new ValidationResult(FormatNotAFileErrorMessage(validationContext.DisplayName));

			string mimeType = MimeTypeHelper.GetMimeType(file.InputStream);
			if (!_validMimeTypes.Contains(mimeType))
				return new ValidationResult(FormatMimeTypeErrorMessage(validationContext.DisplayName));

			return ValidationResult.Success;
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
			ControllerContext context)
		{
			yield return CreateFileMimeTypeValidationRule(FormatMimeTypeErrorMessage(metadata.GetDisplayName()), _validMimeTypes);
		}

		private ModelClientValidationRule CreateFileMimeTypeValidationRule(string errorMessage,
			IEnumerable<string> validMimeTypes)
		{
			ModelClientValidationRule rule = new ModelClientValidationRule
			{
				ErrorMessage = errorMessage,
				ValidationType = "filemimetype",
			};
			rule.ValidationParameters.Add("validmimetypes", string.Join(", ", validMimeTypes));

			return rule;
		}
	}
}