﻿namespace Elevator.PhotoJunk.Site.Validators
{
	using System.ComponentModel.DataAnnotations;
	using Elevator.PhotoJunk.Site.Models.Photo;

	public class PhotoEditModelAttribute : ValidationAttribute
	{
		private const string _notAPhotoErrorMessage = "It is not a photo! How have you made it?";
		private const string _noImageFileErrorMessage = "Please upload an image";
		private const string _imageFileIsUnchangableErrorMessage = "You cannot change the image data of the photo";

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value == null)
				return ValidationResult.Success;

			PhotoEditModel photoEditModel = value as PhotoEditModel;
			if (photoEditModel == null)
				return new ValidationResult(_notAPhotoErrorMessage);

			if (photoEditModel.IsNew && photoEditModel.FullImageFile == null)
				return new ValidationResult(_noImageFileErrorMessage);

			if (!photoEditModel.IsNew && photoEditModel.FullImageFile != null)
				return new ValidationResult(_imageFileIsUnchangableErrorMessage);

			return ValidationResult.Success;
		}
	}
}