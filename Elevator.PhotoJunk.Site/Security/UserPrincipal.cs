﻿namespace Elevator.PhotoJunk.Site.Security
{
	using System.Security.Principal;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class UserPrincipal : IPrincipal
	{
		private UserIndentity userIdentity { get; set; }

		#region IPrincipal Members

		public IIdentity Identity
		{
			get { return userIdentity; }
		}

		public bool IsInRole(string role)
		{
			if (userIdentity.User == null)
			{
				return false;
			}

			return true; //ToDo: actually check if iser is in role
		}

		#endregion

		public UserPrincipal(string name, IUserService userService)
		{
			userIdentity = new UserIndentity();
			userIdentity.Init(name, userService);
		}


		public override string ToString()
		{
			return userIdentity.Name;
		}
	}
}