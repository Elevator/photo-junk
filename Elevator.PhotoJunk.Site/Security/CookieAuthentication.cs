﻿namespace Elevator.PhotoJunk.Site.Security
{
	using System;
	using System.Security.Principal;
	using System.Web;
	using System.Web.Security;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Ninject;

	public class CookieAuthentication : IAuthentication
	{
		private const string cookieName = "__AUTH_COOKIE";

		public HttpContext HttpContext { get; set; }

		[Inject]
		public IUserService UserService { get; set; }

		#region IAuthentication Members

		public User Login(string userName, string password, bool isPersistent)
		{
			User retUser = UserService.Login(userName, password);
			if (retUser != null)
			{
				CreateCookie(userName, isPersistent);
			}
			return retUser;
		}

		public User Login(string userName)
		{
			User retUser = UserService.GetUserByNickname(userName);
			if (retUser != null)
			{
				CreateCookie(userName);
			}
			return retUser;
		}

		private void CreateCookie(string userName, bool isPersistent = false)
		{
			FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
				1,
				userName,
				DateTime.Now,
				DateTime.Now.Add(FormsAuthentication.Timeout),
				isPersistent,
				string.Empty,
				FormsAuthentication.FormsCookiePath);

			// Encrypt the ticket.
			string encTicket = FormsAuthentication.Encrypt(ticket);

			// Create the cookie.
			HttpCookie AuthCookie = new HttpCookie(cookieName)
			{
				Value = encTicket,
				Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
			};
			HttpContext.Response.Cookies.Set(AuthCookie);
		}

		public void LogOut()
		{
			HttpCookie httpCookie = HttpContext.Response.Cookies[cookieName];
			if (httpCookie != null)
			{
				httpCookie.Value = string.Empty;
			}
		}

		private IPrincipal _currentUser;

		public IPrincipal CurrentUser
		{
			get
			{
				if (_currentUser == null)
				{
					try
					{
						HttpCookie authCookie = HttpContext.Request.Cookies.Get(cookieName);
						if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
						{
							FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
							_currentUser = new UserPrincipal(ticket.Name, UserService);
						}
						else
						{
							_currentUser = new UserPrincipal(null, null);
						}
					}
					catch (Exception)
					{
						_currentUser = new UserPrincipal(null, null);
					}
				}
				return _currentUser;
			}
		}

		#endregion
	}
}