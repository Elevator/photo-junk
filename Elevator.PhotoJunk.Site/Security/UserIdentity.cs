﻿namespace Elevator.PhotoJunk.Site.Security
{
	using System.Security.Principal;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class UserIndentity : IIdentity, IUserProvider
	{
		public User User { get; set; }

		public string AuthenticationType
		{
			get { return typeof (User).ToString(); }
		}

		public bool IsAuthenticated
		{
			get { return User != null; }
		}

		public string Name
		{
			get { return User != null ? User.Nickname : "anonym"; }
		}

		public void Init(string name, IUserService userService)
		{
			if (!string.IsNullOrEmpty(name))
			{
				User = userService.GetUserByNickname(name);
			}
		}
	}
}