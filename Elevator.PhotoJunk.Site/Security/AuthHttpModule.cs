﻿namespace Elevator.PhotoJunk.Site.Security
{
	using System;
	using System.Web;
	using Microsoft.Practices.ServiceLocation;

	public class AuthHttpModule : IHttpModule
	{
		public void Init(HttpApplication context)
		{
			context.AuthenticateRequest += Authenticate;
		}

		private void Authenticate(Object source, EventArgs e)
		{
			HttpApplication app = (HttpApplication) source;
			HttpContext context = app.Context;

			IAuthentication auth = ServiceLocator.Current.GetInstance<IAuthentication>();
			auth.HttpContext = context;

			context.User = auth.CurrentUser;
		}

		public void Dispose()
		{
		}
	}
}