﻿namespace Elevator.PhotoJunk.Site.Security
{
	using Elevator.PhotoJunk.Logic.Entities;

	public interface IUserProvider
	{
		User User { get; set; }
	}
}