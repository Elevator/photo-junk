﻿namespace Elevator.PhotoJunk.Site.Loggers
{
	using System;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using log4net.Appender;
	using log4net.Core;
	using Microsoft.Practices.ServiceLocation;
	using Ninject;

	public class DbAppender : AppenderSkeleton
	{
		[Inject]
		public ILogService LogService { get; private set; }

		public DbAppender()
		{
			LogService = ServiceLocator.Current.GetInstance<ILogService>();
		}

		protected override void Append(LoggingEvent loggingEvent)
		{
			LogService.WiteToLog(
				ConvertToLogLevel(loggingEvent.Level), 
				loggingEvent.TimeStamp, 
				loggingEvent.LoggerName,
				ConstructMessage(loggingEvent));
		}

		private string ConstructMessage(LoggingEvent loggingEvent)
		{
			return loggingEvent.RenderedMessage + Environment.NewLine + loggingEvent.GetExceptionString();
		}

		private LogLevel ConvertToLogLevel(Level level)
		{
			switch (level.Name)
			{
				case "FATAL":
					return LogLevel.Fatal;
				case "ERROR":
					return LogLevel.Error;
				case "WARN":
					return LogLevel.Warn;
				case "INFO":
					return LogLevel.Info;
				case "DEBUG":
					return LogLevel.Debug;
				default:
					return LogLevel.Info;
			}
		}
	}
}