﻿namespace Elevator.PhotoJunk.Site.Filters
{
	using System.Web.Mvc;
	using log4net;
	using Ninject;

	public class HandleExceptionFilter : FilterAttribute, IExceptionFilter
	{
		public string View { get; set; }

		[Inject]
		public ILog Log { get; set; }

		public void OnException(ExceptionContext filterContext)
		{
			if (!filterContext.ExceptionHandled)
			{
				Log.Error("Exception has been thrown", filterContext.Exception);
				filterContext.Result = new ViewResult
				{
					ViewName = View
				};
				filterContext.ExceptionHandled = true;
			}
		}
	}
}