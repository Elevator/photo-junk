﻿namespace Elevator.PhotoJunk.Site.DependencyInjection
{
	using System;
	using System.Collections.Generic;
	using Microsoft.Practices.ServiceLocation;
	using Ninject;

	public class NinjectServiceLocator : ServiceLocatorImplBase
	{
		public IKernel Kernel { get; private set; }

		public NinjectServiceLocator(IKernel kernel)
		{
			Kernel = kernel;
		}

		protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
		{
			return Kernel.GetAll(serviceType);
		}

		protected override object DoGetInstance(Type serviceType, string key)
		{
			return Kernel.Get(serviceType, key);
		}
	}
}