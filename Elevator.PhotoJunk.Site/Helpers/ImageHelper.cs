﻿namespace Elevator.PhotoJunk.Site.Helpers
{
	using System;
	using System.Drawing;
	using System.Drawing.Imaging;
	using System.IO;
	using System.Linq;
	using System.Web;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services.Abstract;

	public class ImageHelper
	{
		private readonly string _folderPath;
		private readonly string _folderUrl;
		private readonly IImageService _imageService;
		private readonly int _maxImageSizeKb;
		private readonly string[] _imageMimeTypes;

		public ImageHelper(string folderPath, string folderUrl, IImageService imageService)
		{
			_folderUrl = folderUrl;
			_folderPath = folderPath;
			_imageService = imageService;

			_imageMimeTypes = ConfigHelper.SupportedImageMimeTypes;
			_maxImageSizeKb = ConfigHelper.MaxImageSizeKb;
		}

		public IImage CreateAndSaveImageFromHttpFile(HttpPostedFileBase file)
		{
			int fileSizeKb = (int) (file.InputStream.Length/1024);
			if (fileSizeKb > _maxImageSizeKb)
				throw new ApplicationException("File size limit exceeded");

			string extension = Path.GetExtension(file.FileName);

			Image sysImage = Image.FromStream(file.InputStream);
			ImageFormat format = sysImage.RawFormat;
			ImageCodecInfo codec = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == format.Guid);

			string mimeType = codec.MimeType;
			if (!_imageMimeTypes.Contains(mimeType))
				throw new ApplicationException("File mime-type is not supported");

			Logic.Entities.Image image = new Logic.Entities.Image(Guid.Empty, sysImage.Width, sysImage.Height, mimeType,
				extension);
			MemoryStream stream = new MemoryStream();
			sysImage.Save(stream, sysImage.RawFormat);
			return _imageService.SaveImage(image, stream.ToArray());
		}

		public string GetImageUrl(string imageFilePath)
		{
			string fileName = Path.GetFileName(imageFilePath);
			return _folderUrl + fileName;
		}

		public string GetResizedImageFilePath(IImage image, int height)
		{
			if (image.Height <= height)
				return GetImageFilePath(image);

			string filePath = CalculateResizedImageFilePath(image.IdImage, height, image.FileExtension);

			if (File.Exists(filePath)) return filePath;

			byte[] rawData = _imageService.GetRawData(image.IdImage);

			MemoryStream stream = new MemoryStream(rawData);
			Image sysImage = Image.FromStream(stream);
			Image resizedImage = GetResizedSysImage(sysImage, height);
			PrepareDirectory(filePath);
			resizedImage.Save(filePath);

			return filePath;
		}

		public string GetImageFilePath(IImage image)
		{
			string filePath = CalculateImageFilePath(image.IdImage, image.FileExtension);

			if (File.Exists(filePath)) return filePath;

			byte[] rawData = _imageService.GetRawData(image.IdImage);
			PrepareDirectory(filePath);
			File.WriteAllBytes(filePath, rawData);

			return filePath;
		}

		private static Image GetResizedSysImage(Image image, int height)
		{
			float ratio = image.Width/(float) image.Height;
			int newWidth = (int) (height*ratio);

			return new Bitmap(image, new Size(newWidth, height));
		}

		private string CalculateImageFilePath(Guid idImage, string extension)
		{
			string filename = string.Format("{0}{1}", idImage, extension);
			return Path.Combine(_folderPath, filename);
		}

		private string CalculateResizedImageFilePath(Guid idImage, int height, string extension)
		{
			string filename = string.Format("{0}_{1}{2}", idImage, height, extension);
			return Path.Combine(_folderPath, filename);
		}

		private static void PrepareDirectory(string path)
		{
			string dir = Path.GetDirectoryName(path);

			if (String.IsNullOrWhiteSpace(dir))
				throw new ArgumentException("directory is null or empty");
			if (!Directory.Exists(dir))
				Directory.CreateDirectory(dir);
		}
	}
}