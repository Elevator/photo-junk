﻿namespace Elevator.PhotoJunk.Site.Helpers
{
	using System;
	using System.Web.Configuration;

	public class ConfigHelper
	{
		private static readonly string _imageCacheFolderUrl = WebConfigurationManager.AppSettings["ImageCacheFolderUrl"];
		public static string ImageCacheFolderUrl { get { return _imageCacheFolderUrl; } }

		private static readonly string _noAvatarImageUrl = WebConfigurationManager.AppSettings["NoAvatarImageUrl"];
		public static string NoAvatarImageUrl { get { return _noAvatarImageUrl; } }

		private static readonly string[] _supportedImageMimeTypes = WebConfigurationManager.AppSettings["SupportedImageMimeTypes"].Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
		public static string[] SupportedImageMimeTypes { get { return _supportedImageMimeTypes; } }

		private static readonly int _maxImageSizeKb = int.Parse(WebConfigurationManager.AppSettings["MaxImageSizeKB"]);
		public static int MaxImageSizeKb { get { return _maxImageSizeKb; } }

		private static readonly int _numberOfThumbnailsForSpecializedPage = int.Parse(WebConfigurationManager.AppSettings["NumberOfThumbnailsForSpecializedPage"]);
		public static int NumberOfThumbnailsForSpecializedPage { get { return _numberOfThumbnailsForSpecializedPage; } }

		private static readonly int _numberOfThumbnailsForNonSpecializedPage = int.Parse(WebConfigurationManager.AppSettings["NumberOfThumbnailsForNonSpecializedPage"]);
		public static int NumberOfThumbnailsForNonSpecializedPage { get { return _numberOfThumbnailsForNonSpecializedPage; } }

		private static readonly int _thumbnailHeightPx = int.Parse(WebConfigurationManager.AppSettings["ThumbnailHeightPx"]);
		public static int ThumbnailHeightPx { get { return _thumbnailHeightPx; } }

		private static readonly int _previewHeightPx = int.Parse(WebConfigurationManager.AppSettings["PreviewHeightPx"]);
		public static int PreviewHeightPx { get { return _previewHeightPx; } }

		private static readonly int _maxPhotosForNonPremium = int.Parse(WebConfigurationManager.AppSettings["MaxPhotosForNonPremium"]);
		public static int MaxPhotosForNonPremium { get { return _maxPhotosForNonPremium; } }

		private static readonly int _maxAlbumsForNonPremium = int.Parse(WebConfigurationManager.AppSettings["MaxAlbumsForNonPremium"]);
		public static int MaxAlbumsForNonPremium { get { return _maxAlbumsForNonPremium; } }
	}
}