﻿using Ninject.Web.Common;

namespace Elevator.PhotoJunk.Site
{
	using System.Web.Http;
	using System.Web.Mvc;
	using System.Web.Optimization;
	using System.Web.Routing;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Data.Mapping;
	using Elevator.PhotoJunk.Data.Repositories;
	using Elevator.PhotoJunk.Logic.Helpers;
	using Elevator.PhotoJunk.Logic.Mapping;
	using Elevator.PhotoJunk.Logic.Services;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Binders;
	using Elevator.PhotoJunk.Site.DependencyInjection;
	using Elevator.PhotoJunk.Site.Helpers;
	using Elevator.PhotoJunk.Site.Mapping;
	using Elevator.PhotoJunk.Site.Security;
	using log4net;
	using log4net.Config;
	using Microsoft.Practices.ServiceLocation;
	using Ninject;

	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : NinjectHttpApplication
	{
		private static IKernel _kernel;
		private static string _imagesPath;

		protected override IKernel CreateKernel()
		{
			_kernel = new StandardKernel();

			_kernel.Bind<IAlbumRepository>().To<AlbumRepository>().InSingletonScope();
			_kernel.Bind<IImageRepository>().To<ImageRepository>().InSingletonScope();
			_kernel.Bind<IPhotoRepository>().To<PhotoRepository>().InSingletonScope();
			_kernel.Bind<IUserRepository>().To<UserRepository>().InSingletonScope();
			_kernel.Bind<ILogRepository>().To<LogRepository>().InSingletonScope();
			_kernel.Bind<IVoteRepository>().To<VoteRepository>().InSingletonScope();

			_kernel.Bind<IAlbumService>().To<AlbumService>().InSingletonScope();
			_kernel.Bind<IImageService>().ToConstructor(context => new ImageService(_kernel.Get<IImageRepository>())).InSingletonScope();
			_kernel.Bind<IPhotoService>().To<PhotoService>().InSingletonScope();
			_kernel.Bind<IUserService>().To<UserService>().InSingletonScope();
			_kernel.Bind<ILogService>().To<LogService>().InSingletonScope();
			_kernel.Bind<IVoteService>().To<VoteService>().InSingletonScope();

			_kernel.Bind<ImageHelper>().ToConstructor(context => new ImageHelper(_imagesPath, ConfigHelper.ImageCacheFolderUrl, _kernel.Get<IImageService>())).InSingletonScope();

			_kernel.Bind<IEntityFactory>().To<ProxyFactory>().InSingletonScope();

			_kernel.Bind<IAuthentication>().To<CookieAuthentication>().InRequestScope();

			_kernel.Bind<ILog>().ToMethod(context => LogManager.GetLogger("WebApp")).InRequestScope();

			ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(_kernel));

			XmlConfigurator.Configure();


			return _kernel;
		}

		protected override void OnApplicationStarted()
		{
			base.OnApplicationStarted();
			AreaRegistration.RegisterAllAreas();

			ModelBinders.Binders.Add(typeof (double), new DecimalModelBinder());
			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			_imagesPath = Server.MapPath(ConfigHelper.ImageCacheFolderUrl);

			Mapper.Initialize(cfg =>
			{
				AutomapperDataConfiguration.Configure(cfg);
				AutomapperLogicConfiguration.Configure(cfg);
				AutomapperSiteConfiguration.Configure(cfg);
			});
			Mapper.AssertConfigurationIsValid();
		}
	}
}