﻿namespace Elevator.PhotoJunk.Site.Models.User
{
	using System;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Photo;

	public class UserShowModel
	{
		public Guid Id { get; set; }
		public string Nickname { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleName { get; set; }
		public string Location { get; set; }
		public string Description { get; set; }
		public DateTime RegistrationDate { get; set; }
		public bool IsPremium { get; set; }
		public string AvatarImageFileName { get; set; }

		public PagedData<AlbumThumbnail> Albums { get; set; }
		public PagedData<PhotoThumbnail> Photos { get; set; }
	}
}