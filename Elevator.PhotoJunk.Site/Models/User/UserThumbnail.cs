﻿namespace Elevator.PhotoJunk.Site.Models.User
{
	using System;

	public class UserThumbnail
	{
		public Guid UserId { get; set; }
		public string Nickname { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleName { get; set; }
		public string Location { get; set; }
		public string AvatarImageFileName { get; set; }
	}
}