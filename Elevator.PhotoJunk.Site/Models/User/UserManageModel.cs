﻿namespace Elevator.PhotoJunk.Site.Models.User
{
	using System.ComponentModel.DataAnnotations;
	using System.Web;
	using Elevator.PhotoJunk.Site.Validators;

	public class UserManageModel
	{
		[Required]
		[Display(Name = "Nickname")]
		[DataType(DataType.Text)]
		public string Nickname { get; set; }

		[Display(Name = "First name")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.Text)]
		public string FirstName { get; set; }

		[Display(Name = "Last name")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.Text)]
		public string LastName { get; set; }

		[Display(Name = "Middle name")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.Text)]
		public string MiddleName { get; set; }

		[Display(Name = "Location")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.Text)]
		public string Location { get; set; }

		[Display(Name = "About")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }

		[Required]
		[Display(Name = "IsPremium")]
		public bool IsPremium { get; set; }

		[Display(Name = "Image file")]
		[FileSize("MaxImageSizeKB")]
		[FileMimeType("SupportedImageMimeTypes")]
		public HttpPostedFileBase AvatarImageFile { get; set; }
		public string AvatarImageFileName { get; set; }
	}
}