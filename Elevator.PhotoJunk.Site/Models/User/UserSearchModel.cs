﻿namespace Elevator.PhotoJunk.Site.Models.User
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public class UserSearchModel
	{
		[Display(Name = "Name")]
		[DataType(DataType.Text)]
		public string Name { get; set; }

		[Display(Name = "Email")]
		[DataType(DataType.Text)]
		public string Email { get; set; }

		[Display(Name = "Location")]
		[DataType(DataType.Text)]
		public string Location { get; set; }

		[Display(Name = "About")]
		[DataType(DataType.Text)]
		public string Description { get; set; }

		[Display(Name = "Registered from")]
		[DataType(DataType.DateTime)]
		public DateTime? RegistrationPeriodStart { get; set; }

		[Display(Name = "to")]
		[DataType(DataType.DateTime)]
		public DateTime? RegistrationPeriodEnd { get; set; }

		[Display(Name = "Premium")]
		public bool IsPremium { get; set; }
	}
}