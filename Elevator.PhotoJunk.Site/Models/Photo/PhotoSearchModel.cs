﻿namespace Elevator.PhotoJunk.Site.Models.Photo
{
	using System.ComponentModel.DataAnnotations;
	using Elevator.PhotoJunk.Logic.Pagination;

	public class PhotoSearchModel
	{
		[Display(Name = "Title")]
		[DataType(DataType.Text)]
		public string Title { get; set; }

		[Display(Name = "Descriprion")]
		[DataType(DataType.MultilineText)]
		public string Descriprion { get; set; }

		[Display(Name = "Place")]
		[DataType(DataType.Text)]
		public string Place { get; set; }

		[Display(Name = "Camera")]
		[DataType(DataType.Text)]
		public string Camera { get; set; }

		[Display(Name = "Author name")]
		[DataType(DataType.Text)]
		public string AuthorName { get; set; }
	}
}