﻿namespace Elevator.PhotoJunk.Site.Models.Photo
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.Web;
	using Elevator.PhotoJunk.Site.Validators;

	[PhotoEditModel]
	public class PhotoEditModel
	{
		public bool IsNew { get; set; }

		public Guid Id { get; set; }
		public Guid AuthorId { get; set; }

		[Required]
		[Display(Name = "Title")]
		[DataType(DataType.Text)]
		public string Title { get; set; }

		[Display(Name = "Place")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.Text)]
		public string Place { get; set; }

		[Display(Name = "Description")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }

		[Display(Name = "Camera")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.Text)]
		public string Camera { get; set; }

		[Display(Name = "Focal length")]
		[DataType(DataType.Text)]
		public double FocalLength { get; set; }

		[Display(Name = "Aperture size")]
		[DataType(DataType.Text)]
		public double ApertureSize { get; set; }

		[Display(Name = "Iso speed")]
		[DataType(DataType.Text)]
		public int IsoSpeed { get; set; }

		[Display(Name = "Flash")]
		public bool Flash { get; set; }

		[Display(Name = "Image")]
		[FileSize("MaxImageSizeKB")]
		[FileMimeType("SupportedImageMimeTypes")]
		public HttpPostedFileBase FullImageFile { get; set; }
		public string ImageFileName { get; set; }
		public string FullImageFileName { get; set; }

	}
}