﻿namespace Elevator.PhotoJunk.Site.Models.Photo
{
	using Elevator.PhotoJunk.Logic.Pagination;

	public class UserPhotosModel
	{
		public string UserName { get; set; }
		public PagedData<PhotoThumbnail> Photos { get; set; }
	}
}