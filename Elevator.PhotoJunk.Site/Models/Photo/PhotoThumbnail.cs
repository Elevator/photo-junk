﻿namespace Elevator.PhotoJunk.Site.Models.Photo
{
	using System;

	public class PhotoThumbnail
	{
		public Guid PhotoId { get; set; }
		public string AuthorName { get; set; }
		public string AuthorEmail { get; set; }
		public string Title { get; set; }
		public string ImageFileName { get; set; }
	}
}