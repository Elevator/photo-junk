﻿namespace Elevator.PhotoJunk.Site.Models.Photo
{
	using System;

	public class PhotoShowModel
	{
		public Guid Id { get; set; }
		public string AuthorName { get; set; }
		public string AuthorEmail { get; set; }
		public string Title { get; set; }
		public string Place { get; set; }
		public DateTime UploadDate { get; set; }

		public string Description { get; set; }
		public string Camera { get; set; }
		public double FocalLength { get; set; }
		public double ApertureSize { get; set; }
		public int IsoSpeed { get; set; }
		public bool Flash { get; set; }

		public int Likes { get; set; }
		public int Dislikes { get; set; }

		public bool CanVote { get; set; }

		public string ImageFileName { get; set; }
		public string FullImageFileName { get; set; }
	}
}