﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using System;
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Site.Models.Photo;

	public class AlbumShowModel
	{
		public Guid Id { get; set; }
		public string AuthorName { get; set; }
		public string AuthorEmail { get; set; }
		public Guid AuthorId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime UploadDate { get; set; }
		public string CoverImageFileName { get; set; }

		public PagedData<PhotoThumbnail> Photos { get; set; }
	}
}