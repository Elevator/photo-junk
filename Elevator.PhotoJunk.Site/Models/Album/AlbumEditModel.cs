﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.Web;
	using Elevator.PhotoJunk.Site.Validators;

	[AlbumEditModel]
	public class AlbumEditModel
	{
		public bool IsNew { get; set; }

		public Guid Id { get; set; }
		public Guid AuthorId { get; set; }

		[Required]
		[Display(Name = "Title")]
		[DataType(DataType.Text)]
		public string Title { get; set; }

		[Display(Name = "Description")]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }

		[Display(Name = "Cover")]
		[FileSize("MaxImageSizeKB")]
		[FileMimeType("SupportedImageMimeTypes")]
		public HttpPostedFileBase CoverImageFile { get; set; }
		public string CoverImageFileName { get; set; }
	}
}