﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using System;

	public class AlbumThumbnail
	{
		public Guid AlbumId { get; set; }
		public string AuthorName { get; set; }
		public string AuthorEmail { get; set; }
		public string Title { get; set; }
		public string CoverFileName { get; set; }
	}
}