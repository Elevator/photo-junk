﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using Elevator.PhotoJunk.Site.Models.Photo;

	public class PhotoCheck
	{
		public PhotoThumbnail Photo { get; set; }
		public bool Checked { get; set; }
	}
}