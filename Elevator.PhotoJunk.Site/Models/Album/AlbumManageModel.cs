﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using System.Collections.Generic;

	public class AlbumManageModel
	{
		public AlbumThumbnail Album { get; set; }
		public List<PhotoCheck> Photos { get; set; }
	}
}