﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using System.ComponentModel.DataAnnotations;

	public class AlbumSearchModel
	{
		[Display(Name = "Title")]
		[DataType(DataType.Text)]
		public string Title { get; set; }

		[Display(Name = "Descriprion")]
		[DataType(DataType.MultilineText)]
		public string Descriprion { get; set; }

		[Display(Name = "Author name")]
		[DataType(DataType.Text)]
		public string AuthorName { get; set; }
	}
}