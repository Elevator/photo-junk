﻿namespace Elevator.PhotoJunk.Site.Models.Album
{
	using Elevator.PhotoJunk.Logic.Pagination;

	public class UserAlbumsModel
	{
		public string UserName { get; set; }
		public PagedData<AlbumThumbnail> Albums { get; set; }
	}
}