﻿namespace Elevator.PhotoJunk.Site.Models.Home
{
	using Elevator.PhotoJunk.Logic.Pagination;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Elevator.PhotoJunk.Site.Models.Album;
	using Elevator.PhotoJunk.Site.Models.Photo;
	using Elevator.PhotoJunk.Site.Models.User;

	public class IndexModel
	{
		public PagedData<UserThumbnail> Authors;
		public PagedData<PhotoThumbnail> Photos;
		public PagedData<AlbumThumbnail> Albums;
	}
}