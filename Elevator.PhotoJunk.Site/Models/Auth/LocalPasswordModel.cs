namespace Elevator.PhotoJunk.Site.Models.Auth
{
	using System.ComponentModel.DataAnnotations;

	public class LocalPasswordModel
	{
		[Required]
		[Display(Name = "Current password")]
		[DataType(DataType.Password)]
		public string OldPassword { get; set; }

		[Required]
		[Display(Name = "New password")]
		[DataType(DataType.Password)]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		public string NewPassword { get; set; }

		[Display(Name = "Confirm new password")]
		[DataType(DataType.Password)]
		[Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
		public string ConfirmPassword { get; set; }
	}
}