namespace Elevator.PhotoJunk.Site.Models.Auth
{
	using System.ComponentModel.DataAnnotations;

	public class RegisterModel
	{
		[Required]
		[Display(Name = "Email")]
		[DataType(DataType.Password)]
		public string Email { get; set; }

		[Required]
		[Display(Name = "User name")]
		[DataType(DataType.Text)]
		public string UserName { get; set; }

		[Required]
		[Display(Name = "Password")]
		[DataType(DataType.Password)]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Confirm password")]
		[DataType(DataType.Password)]
		[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
		public string ConfirmPassword { get; set; }
	}
}