﻿namespace Elevator.PhotoJunk.Data.Mapping.Profiles
{
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;

	public class ImageProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<ImageSelectMetaInfo_Result, Image>()
				.ForMember(x => x.IdImage, m => m.MapFrom(s => s.IdImage))
				.ForMember(x => x.Width, m => m.MapFrom(s => s.Width))
				.ForMember(x => x.Height, m => m.MapFrom(s => s.Height))
				.ForMember(x => x.MimeType, m => m.MapFrom(s => s.MimeType))
				.ForMember(x => x.FileExtension, m => m.MapFrom(s => s.FileExtension));
		}
	}
}