﻿namespace Elevator.PhotoJunk.Data.Mapping.Profiles
{
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;

	public class PhotoProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<PhotoSelect_Result, Photo>()
				.ForMember(x => x.IdPhoto, m => m.MapFrom(s => s.IdPhoto))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.Place, m => m.MapFrom(s => s.Place))
				.ForMember(x => x.Camera, m => m.MapFrom(s => s.Camera))
				.ForMember(x => x.FocalLength, m => m.MapFrom(s => s.FocalLength))
				.ForMember(x => x.ApertureSize, m => m.MapFrom(s => s.ApertureSize))
				.ForMember(x => x.IsoSpeed, m => m.MapFrom(s => s.IsoSpeed))
				.ForMember(x => x.Flash, m => m.MapFrom(s => s.Flash))
				.ForMember(x => x.IdAuthor, m => m.MapFrom(s => s.IdAuthor))
				.ForMember(x => x.IdImage, m => m.MapFrom(s => s.IdImage))
				.ForMember(x => x.UploadDate, m => m.MapFrom(s => s.UploadDate));
		}
	}
}