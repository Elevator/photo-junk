﻿namespace Elevator.PhotoJunk.Data.Mapping.Profiles
{
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;

	public class AlbumProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<AlbumSelect_Result, Album>()
				.ForMember(x => x.IdAlbum, m => m.MapFrom(s => s.IdAlbum))
				.ForMember(x => x.Title, m => m.MapFrom(s => s.Title))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.IdAuthor, m => m.MapFrom(s => s.IdAuthor))
				.ForMember(x => x.IdCover, m => m.MapFrom(s => s.IdCover))
				.ForMember(x => x.UploadDate, m => m.MapFrom(s => s.UploadDate));
		}
	}
}