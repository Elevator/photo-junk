﻿namespace Elevator.PhotoJunk.Data.Mapping.Profiles
{
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;

	public class UserProfile : Profile
	{
		protected override void Configure()
		{
			Mapper.CreateMap<UserSelect_Result, User>()
				.ForMember(x => x.IdUser, m => m.MapFrom(s => s.IdUser))
				.ForMember(x => x.Email, m => m.MapFrom(s => s.Email))
				.ForMember(x => x.Password, m => m.MapFrom(s => s.Password))
				.ForMember(x => x.Nickname, m => m.MapFrom(s => s.Nickname))
				.ForMember(x => x.FirstName, m => m.MapFrom(s => s.FirstName))
				.ForMember(x => x.LastName, m => m.MapFrom(s => s.LastName))
				.ForMember(x => x.MiddleName, m => m.MapFrom(s => s.MiddleName))
				.ForMember(x => x.Location, m => m.MapFrom(s => s.Location))
				.ForMember(x => x.Description, m => m.MapFrom(s => s.Description))
				.ForMember(x => x.RegistrationDate, m => m.MapFrom(s => s.RegistrationDate))
				.ForMember(x => x.IsPremium, m => m.MapFrom(s => s.IsPremium));
		}
	}
}