﻿using AutoMapper.Configuration;

namespace Elevator.PhotoJunk.Data.Mapping
{
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Mapping.Profiles;

	public static class AutomapperDataConfiguration
	{
		public static void Configure(IConfiguration configuration)
		{
			configuration.AddProfile(new UserProfile());
			configuration.AddProfile(new ImageProfile());
			configuration.AddProfile(new PhotoProfile());
			configuration.AddProfile(new AlbumProfile());
		}
	}
}