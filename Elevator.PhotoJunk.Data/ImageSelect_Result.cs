//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Elevator.PhotoJunk.Data
{
    using System;
    
    public partial class ImageSelect_Result
    {
        public System.Guid IdImage { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public byte[] RawData { get; set; }
        public string MimeType { get; set; }
        public string FileExtension { get; set; }
    }
}
