﻿namespace Elevator.PhotoJunk.Data.Repositories
{
	using System;
	using System.Linq;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;
	using Elevator.PhotoJunk.Data.Core.Repositories;

	public class ImageRepository : IImageRepository
	{
		public Image Create(Image image, byte[] rawData)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				Guid? id = context.ImageCreate(image.Width, image.Height, image.MimeType, image.FileExtension, rawData).First();
				if (!id.HasValue) throw new ApplicationException("Cannot create entity " + image);

				image.IdImage = id.Value;
				return image;
			}
		}

		public void Delete(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.ImageDelete(id);
			}
		}

		public Image GetById(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ImageSelectMetaInfo_Result result = context.ImageSelectMetaInfoById(id).FirstOrDefault();
				return result == null ? null : Mapper.Map<ImageSelectMetaInfo_Result, Image>(result);
			}
		}

		public byte[] GetRawDataById(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				return context.ImageSelectRawDataById(id).FirstOrDefault();
			}
		}
	}
}