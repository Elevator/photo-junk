﻿using System.Data.Entity.Core.Objects;

namespace Elevator.PhotoJunk.Data.Repositories
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;
	using Elevator.PhotoJunk.Data.Core.Repositories;

	public class UserRepository : IUserRepository
	{
		public User Create(User user)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				Guid? id = context.UserCreate(user.Email, user.Password, user.Nickname, user.FirstName, user.LastName, user.MiddleName,
					user.Location, user.Description, user.IsPremium).First();

				if (!id.HasValue) throw new ApplicationException("Cannot create entity " + user);

				user.IdUser = id.Value;
				return user;
			}
		}

		public void Update(User user)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.UserUpdate(user.IdUser, user.Email, user.Password, user.Nickname, user.FirstName, user.LastName, user.MiddleName,
					user.Location, user.Description, user.IsPremium);
			}
		}

		public void Delete(string email)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.UserDelete(email);
			}
		}

		public User GetByNickname(string nickname)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				UserSelect_Result result = context.UserSelectByNickname(nickname).FirstOrDefault();
				return result == null ? null : Mapper.Map<UserSelect_Result, User>(result);
			}
		}

		public User GetByEmail(string email)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				UserSelect_Result result = context.UserSelectByEmail(email).FirstOrDefault();
				return result == null ? null : Mapper.Map<UserSelect_Result, User>(result);
			}
		}

		public User GetById(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				UserSelect_Result result = context.UserSelectById(id).FirstOrDefault();
				return result == null ? null : Mapper.Map<UserSelect_Result, User>(result);
			}
		}

		public IEnumerable<User> GetAll()
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				return context.UserSelectAll().Select(Mapper.Map<UserSelect_Result, User>).ToList();
			}
		}

		public IEnumerable<User> GetPage(int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<User> result = context.UserSelectPage(pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<UserSelect_Result, User>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<User> Filter(string email, string nickname, string firstName, string middleName, string lastName,
			string location, string description, DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd, bool? premium, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<User> result = context.UserFilter(email, nickname, firstName, lastName, middleName, location, description, registrationPeriodStart, registrationPeriodEnd, premium, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<UserSelect_Result, User>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<User> FilterByAnyName(string email, string anyName, string location, string description, DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd,
			bool? premium, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<User> result = context.UserFilterByAnyName(email, anyName, location, description, registrationPeriodStart, registrationPeriodEnd, premium, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<UserSelect_Result, User>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<User> FilterByKeywords(string keywords, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<User> result = context.UserFilterByKeywords(keywords, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<UserSelect_Result, User>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public void SetUserAvatar(Guid userId, Guid imageId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.AvatarSet(userId, imageId);
			}
		}

		public bool UserAvatarExist(Guid userId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				Guid? userAvatarId = context.AvatarGet(userId).FirstOrDefault();
				return userAvatarId.HasValue;
			}
		}

		public Guid GetUserAvatar(Guid userId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				Guid? userAvatarId = context.AvatarGet(userId).FirstOrDefault();

				if (!userAvatarId.HasValue)
					throw new ApplicationException("Cannot find avatar for user " + userId);

				return userAvatarId.Value;
			}
		}

		public void RemoveUserAvatar(Guid userId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.AvatarRemove(userId);
			}
		}
	}
}