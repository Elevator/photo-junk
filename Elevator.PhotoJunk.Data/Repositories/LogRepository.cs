﻿namespace Elevator.PhotoJunk.Data.Repositories
{
	using System;
	using Elevator.PhotoJunk.Data.Core.Repositories;

	public class LogRepository : ILogRepository
	{
		public void WiteToLog(string level, DateTime timeStamp, string source, string message)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.WriteToLog(level, timeStamp, source, message);
			}
		}
	}
}