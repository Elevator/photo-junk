﻿namespace Elevator.PhotoJunk.Data.Repositories
{
	using System;
	using System.Linq;
	using Elevator.PhotoJunk.Data.Core.Repositories;

	public class VoteRepository: IVoteRepository
	{
		public void Vote(Guid userId, Guid photoId, int vote)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.VotePhoto(userId, photoId, vote);
			}
		}

		public int GetPositiveVotesForPhoto(Guid photoId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				int? votes = context.VoteGetPositiveForPhoto(photoId).FirstOrDefault();
				return votes.HasValue ? votes.Value : 0;
			}
		}

		public int GetNegativeVotesForPhoto(Guid photoId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				int? votes = context.VoteGetNegativeForPhoto(photoId).FirstOrDefault();
				return votes.HasValue ? votes.Value : 0;
			}
		}

		public int GetVote(Guid userId, Guid photoId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				int? vote = context.VoteGet(userId, photoId).FirstOrDefault();
				return vote.HasValue ? vote.Value : 0;
			}
		}
	}
}