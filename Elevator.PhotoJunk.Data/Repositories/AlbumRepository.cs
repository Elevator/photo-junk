﻿using System.Data.Entity.Core.Objects;

namespace Elevator.PhotoJunk.Data.Repositories
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;
	using Elevator.PhotoJunk.Data.Core.Repositories;

	public class AlbumRepository : IAlbumRepository
	{
		public Album Create(Album album)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				Guid? id = context.AlbumCreate(album.IdAuthor, album.Title, album.Description, album.IdCover).First();
				if (!id.HasValue) throw new ApplicationException("Cannot create entity " + album);

				album.IdAlbum = id.Value;
				return album;
			}
		}

		public void Update(Album album)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.AlbumUpdate(album.IdAlbum, album.IdAuthor, album.Title, album.Description, album.IdCover);
			}
		}

		public void Delete(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.AlbumDelete(id);
			}
		}

		public Album GetById(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				AlbumSelect_Result result = context.AlbumSelectById(id).FirstOrDefault();
				return result == null ? null : Mapper.Map<AlbumSelect_Result, Album>(result);
			}
		}

		public Album GetByTitle(string title)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				AlbumSelect_Result result = context.AlbumSelectByTitle(title).FirstOrDefault();
				return result == null ? null : Mapper.Map<AlbumSelect_Result, Album>(result);
			}
		}

		public IEnumerable<Album> GetAll()
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				return context.AlbumSelectAll().Select(Mapper.Map<AlbumSelect_Result, Album>).ToList();
			}
		}

		public IEnumerable<Album> GetPage(int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Album> result = context.AlbumSelectPage(pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<AlbumSelect_Result, Album>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Album> FilterByAuthor(Guid userId, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Album> result = context.AlbumFilterByAuthor(userId, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<AlbumSelect_Result, Album>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Album> Filter(string keyWords, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Album> result = context.AlbumFilterByKeywords(keyWords, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<AlbumSelect_Result, Album>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Album> Filter(string title, string descriprion, string authorName, int pageNumber,
			int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Album> result = context.AlbumFilter(title, descriprion, authorName, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<AlbumSelect_Result, Album>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}
	}
}