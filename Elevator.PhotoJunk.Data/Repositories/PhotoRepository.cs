﻿using System.Data.Entity.Core.Objects;

namespace Elevator.PhotoJunk.Data.Repositories
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using AutoMapper;
	using Elevator.PhotoJunk.Data.Core.Dto;
	using Elevator.PhotoJunk.Data.Core.Repositories;

	public class PhotoRepository : IPhotoRepository
	{
		public Photo Create(Photo photo)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				Guid? id = context.PhotoCreate(photo.Title,
					photo.Description, photo.Place, photo.Camera, photo.FocalLength, photo.ApertureSize, photo.IsoSpeed, photo.Flash,
					photo.IdAuthor, photo.IdImage).First();
				if (!id.HasValue) throw new ApplicationException("Cannot create entity " + photo);

				photo.IdPhoto = id.Value;
				return photo;
			}
		}

		public void Update(Photo photo)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.PhotoUpdate(photo.IdPhoto, photo.Title,
					photo.Description, photo.Place, photo.Camera, photo.FocalLength, photo.ApertureSize, photo.IsoSpeed, photo.Flash,
					photo.IdAuthor, photo.IdImage);
			}
		}

		public void Delete(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.PhotoDelete(id);
			}
		}

		public Photo GetById(Guid id)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				PhotoSelect_Result result = context.PhotoSelectById(id).FirstOrDefault();
				return result == null ? null : Mapper.Map<PhotoSelect_Result, Photo>(result);
			}
		}

		public IEnumerable<Photo> GetAll()
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				return context.PhotoSelectAll().Select(Mapper.Map<PhotoSelect_Result, Photo>).ToList();
			}
		}

		public IEnumerable<Photo> GetPage(int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Photo> result = context.PhotoSelectPage(pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<PhotoSelect_Result, Photo>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Photo> Filter(string title, string descriprion, string place, string camera, string authorName, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Photo> result = context.PhotoFilter(title, descriprion, place, camera, authorName, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<PhotoSelect_Result, Photo>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Photo> FilterByAuthor(Guid userId, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Photo> result = context.PhotoFilterByAuthor(userId, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<PhotoSelect_Result, Photo>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Photo> FilterByAlbum(Guid albumId, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Photo> result = context.PhotoFilterByAlbum(albumId, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<PhotoSelect_Result, Photo>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public IEnumerable<Photo> FilterByKeywords(string keywords, int pageNumber, int itemsPerPage, out int itemsTotal)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				ObjectParameter itemsTotalOut = new ObjectParameter("itemsTotalOut", typeof(int));
				List<Photo> result = context.PhotoFilterByKeywords(keywords, pageNumber, itemsPerPage, itemsTotalOut).Select(Mapper.Map<PhotoSelect_Result, Photo>).ToList();
				itemsTotal = (int)itemsTotalOut.Value;
				return result;
			}
		}

		public void AddPhotoToAlbum(Guid photoId, Guid albumId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.AlbumPhotoAdd(photoId, albumId);
			}
		}

		public void RemovePhotoFromAlbum(Guid photoId, Guid albumId)
		{
			using (PhotoJunkEntities context = new PhotoJunkEntities())
			{
				context.AlbumPhotoRemove(photoId, albumId);
			}
		}
	}
}