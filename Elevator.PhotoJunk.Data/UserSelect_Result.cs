//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Elevator.PhotoJunk.Data
{
    using System;
    
    public partial class UserSelect_Result
    {
        public System.Guid IdUser { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public bool IsPremium { get; set; }
        public bool IsDeleted { get; set; }
        public string Nickname { get; set; }
        public System.DateTime RegistrationDate { get; set; }
    }
}
