﻿namespace Elevator.PhotoJunk.Loader
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public class Option
	{
		private readonly Dictionary<string, Option> _children = new Dictionary<string, Option>();

		public Action<IEnumerable<string>> Action { get; set; }

		public Option(Action<IEnumerable<string>> action = null)
		{
			Action = action;
		}

		public void AddChild(string command, Option child)
		{
			_children[command] = child;
		}

		public Option GetChild(string name)
		{
			return !_children.ContainsKey(name) ? null : _children[name];
		}

		public void Execute(IEnumerable<string> commands)
		{
			if (!commands.Any())
			{
				if (Action == null)
					throw new ArgumentException(string.Format("Action not specified for this command"));
				
				Action(commands);
				return;
			}

			string command = commands.First();
			Option child = GetChild(command);
			if (child != null)
			{
				IEnumerable<string> commandsRest = commands.Skip(1);
				child.Execute(commandsRest);
			}
			else
			{
				if (Action == null)
					throw new ArgumentException(string.Format("wrong command: {0}", command));

				Action(commands);
			}
		}
	}
}