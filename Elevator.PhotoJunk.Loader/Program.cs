﻿namespace Elevator.PhotoJunk.Loader
{
	using System;
	using System.Collections.Generic;
	using System.Drawing.Imaging;
	using System.IO;
	using System.Linq;
	using Elevator.Logging;
	using Elevator.PhotoJunk.Data.Core.Repositories;
	using Elevator.PhotoJunk.Data.Repositories;
	using Elevator.PhotoJunk.Logic.Entities;
	using Elevator.PhotoJunk.Logic.Entities.Abstract;
	using Elevator.PhotoJunk.Logic.Services;
	using Elevator.PhotoJunk.Logic.Services.Abstract;
	using Newtonsoft.Json;
	using Ninject;
	using AlbumDto = Elevator.PhotoJunk.Data.Core.Dto.Album;
	using ImageDto = Elevator.PhotoJunk.Data.Core.Dto.Image;
	using PhotoDto = Elevator.PhotoJunk.Data.Core.Dto.Photo;
	using UserDto = Elevator.PhotoJunk.Data.Core.Dto.User;
	using SysImage = System.Drawing.Image;

	internal class Program
	{
		private static readonly IKernel Kernel = new StandardKernel();
		private static bool _exit;

		private static void ConfigureKernel()
		{
			Kernel.Bind<IAlbumRepository>().To<AlbumRepository>();
			Kernel.Bind<IImageRepository>().To<ImageRepository>();
			Kernel.Bind<IPhotoRepository>().To<PhotoRepository>();
			Kernel.Bind<IUserRepository>().To<UserRepository>();
			Kernel.Bind<ILogRepository>().To<LogRepository>();

			Kernel.Bind<IImageService>()
				.ToConstructor(ctorArg => new ImageService(ctorArg.Inject<IImageRepository>()));
			Kernel.Bind<IAlbumService>().To<AlbumService>();
			Kernel.Bind<IPhotoService>().To<PhotoService>();
			Kernel.Bind<IUserService>().To<UserService>();
			Kernel.Bind<ILogService>().To<LogService>();
		}

		private static Option ConfigureOptions()
		{
			OptionBuilder builder = new OptionBuilder();
			builder.AddOption("image create", args => CreateImage(args.First()));
			builder.AddOption("image create directory", args => CreateImages(args.First()));
			builder.AddOption("image show", args => ShowImage(args.First()));
			builder.AddOption("image delete", args => DeleteImage(args.First()));

			builder.AddOption("photo create from json", args => CreatePhoto(args.First()));
			builder.AddOption("photo create from file", args => CreatePhotoFromFile(args.First()));
			builder.AddOption("photos create from json", args => CreatePhotos(args.First()));
			builder.AddOption("photos create from file", args => CreatePhotosFromFile(args.First()));
			builder.AddOption("photo show", args => ShowPhoto(args.First()));
			builder.AddOption("photo update", args => UpdatePhoto(args.First()));
			builder.AddOption("photo delete", args => DeletePhoto(args.First()));

			builder.AddOption("album create from json", args => CreatePhoto(args.First()));
			builder.AddOption("album create from file", args => CreatePhotoFromFile(args.First()));
			builder.AddOption("albums create from json", args => CreatePhotos(args.First()));
			builder.AddOption("albums create from file", args => CreatePhotosFromFile(args.First()));
			builder.AddOption("photo show", args => ShowPhoto(args.First()));
			builder.AddOption("photo update", args => UpdatePhoto(args.First()));
			builder.AddOption("photo delete", args => DeletePhoto(args.First()));

			builder.AddOption("exit", args => _exit = true);

			return builder.GetOption();
		}

		private static void Main(string[] args)
		{
			ConfigureKernel();
			Option option = ConfigureOptions();

			if (args.Length != 0)
			{
				try
				{
					option.Execute(args);
				}
				catch (Exception e)
				{
					Log.Exception(e);
				}				
			}
			else
			{
				while (!_exit)
				{
					try
					{
						string line = Console.ReadLine();
						if (string.IsNullOrWhiteSpace(line))
							throw new ArgumentException("argument list is empty");

						args = line.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);

						option.Execute(args);
					}
					catch (Exception e)
					{
						Log.Exception(e);
					}
				}
			}
		}

		private static void CreateImages(string directoryPath)
		{
			foreach (string filename in Directory.EnumerateFiles(directoryPath))
			{
				try
				{
					CreateImage(filename);
				}
				catch (Exception ex)
				{
					Log.Exception(ex);
				}
			}
		}

		private static void CreateImage(string filename)
		{
			Log.Info("Creating image: {0}", filename);

			IImageService imageService = Kernel.Get<IImageService>();

			SysImage sysImage = SysImage.FromFile(filename);
			ImageFormat format = sysImage.RawFormat;
			ImageCodecInfo codec = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == format.Guid);

			string mimeType = codec.MimeType;
			string extension = Path.GetExtension(filename);

			IImage image = new Image(Guid.Empty, sysImage.Width, sysImage.Height, mimeType, extension);

			MemoryStream stream = new MemoryStream();
			sysImage.Save(stream, sysImage.RawFormat);
			image = imageService.SaveImage(image, stream.ToArray());

			Log.Info("Image created full for {0}: {1}", filename, image.IdImage);
		}

		private static void ShowImage(string guid)
		{
			IImageService imageService = Kernel.Get<IImageService>();
			IImage image = imageService.GetImage(Guid.Parse(guid));
			string imageJson = JsonConvert.SerializeObject(image, Formatting.Indented);
			Log.Info("Image:");
			Log.Info(imageJson);
		}

		private static void DeleteImage(string guid)
		{
			IImageService imageService = Kernel.Get<IImageService>();
			imageService.DeleteImage(Guid.Parse(guid));
			Log.Info("Image {0} deleted", guid);
		}

		private static void ShowUser(string guid)
		{
			IUserService userService = Kernel.Get<IUserService>();
			IUser user = userService.GetUserById(Guid.Parse(guid));
			string userJson = JsonConvert.SerializeObject(user, Formatting.Indented);
			Log.Info("User:");
			Log.Info(userJson);
		}

		private static void CreatePhotoFromFile(string filename)
		{
			string json = File.ReadAllText(filename);
			CreatePhoto(json);
		}

		private static void CreatePhotosFromFile(string filename)
		{
			string json = File.ReadAllText(filename);
			CreatePhotos(json);
		}

		private static void CreatePhotos(string json)
		{
			IPhotoRepository photoRepository = Kernel.Get<IPhotoRepository>();
			List<PhotoDto> photos = JsonConvert.DeserializeObject<List<PhotoDto>>(json);
			foreach (PhotoDto photo in photos)
			{
				PhotoDto resultPhoto = photoRepository.Create(photo);
				Log.Info("Photo {0} created", resultPhoto.IdPhoto);
			}
		}

		private static void CreatePhoto(string json)
		{
			PhotoDto photo = JsonConvert.DeserializeObject<PhotoDto>(json);
			IPhotoRepository photoRepository = Kernel.Get<IPhotoRepository>();
			photo = photoRepository.Create(photo);
			Log.Info("Photo {0} created", photo.IdPhoto);
		}

		private static void ShowPhoto(string guid)
		{
			IPhotoRepository photoRepository = Kernel.Get<IPhotoRepository>();
			PhotoDto photo = photoRepository.GetById(Guid.Parse(guid));
			string photoJson = JsonConvert.SerializeObject(photo, Formatting.Indented);
			Log.Info("Photo:");
			Log.Info(photoJson);
		}

		private static void UpdatePhoto(string json)
		{
			PhotoDto photo = JsonConvert.DeserializeObject<PhotoDto>(json);
			IPhotoRepository photoRepository = Kernel.Get<IPhotoRepository>();
			photoRepository.Update(photo);
			Log.Info("Photo {0} updated", photo.IdPhoto);
		}

		private static void DeletePhoto(string guid)
		{
			IPhotoRepository photoRepository = Kernel.Get<IPhotoRepository>();
			photoRepository.Delete(Guid.Parse(guid));
			Log.Info("Photo {0} deleted", guid);
		}
	}
}