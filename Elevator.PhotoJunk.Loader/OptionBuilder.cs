﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elevator.PhotoJunk.Loader
{
	public class OptionBuilder
	{
		private readonly Option _rootOption = new Option();

		public void AddOption(string line, Action<IEnumerable<string>> action)
		{
			IEnumerable<string> commands = line.Split(new char[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
			if (!commands.Any())
				throw new ArgumentException("argument list is empty");

			AddSubOption(_rootOption, commands, action);
		}

		public Option GetOption()
		{
			return _rootOption;
		}

		private void AddSubOption(Option node, IEnumerable<string> commands, Action<IEnumerable<string>> action)
		{
			if (!commands.Any())
				throw new ArgumentException("argument list is empty");

			string command = commands.First();
			IEnumerable<string> commandsRest = commands.Skip(1);

			Option child = node.GetChild(command);

			if (commandsRest.Any())
			{
				if (child == null)
				{
					child = new Option();
					node.AddChild(command, child);
				}

				AddSubOption(child, commandsRest, action);
			}
			else
			{
				if (child == null)
				{
					child = new Option(action);
					node.AddChild(command, child);
				}
				else
				{
					child.Action = action;
				}
			}
		}
	}
}
