﻿using System;
using Elevator.PhotoJunk.Data.Core.Dto;

namespace Elevator.PhotoJunk.Data.Core.Repositories
{
	public interface IImageRepository
	{
		Image Create(Image image, byte[] rawData);
		void Delete(Guid id);

		Image GetById(Guid id);
		byte[] GetRawDataById(Guid id);
	}
}