﻿using System;
using System.Collections.Generic;
using Elevator.PhotoJunk.Data.Core.Dto;

namespace Elevator.PhotoJunk.Data.Core.Repositories
{
	public interface IUserRepository
	{
		User Create(User user);
		void Update(User user);
		void Delete(string email);

		User GetByNickname(string nickname);
		User GetByEmail(string email);
		User GetById(Guid id);
		IEnumerable<User> GetAll();

		IEnumerable<User> GetPage(int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<User> Filter(string email, string nickname, string firstName, string middleName, string lastName, string location, string description, DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd, bool? premium, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<User> FilterByAnyName(string email, string anyName, string location, string description, DateTime? registrationPeriodStart, DateTime? registrationPeriodEnd, bool? premium, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<User> FilterByKeywords(string keywords, int pageNumber, int itemsPerPage, out int itemsTotal);

		void SetUserAvatar(Guid userId, Guid imageId);
		Guid GetUserAvatar(Guid userId);
		bool UserAvatarExist(Guid userId);
		void RemoveUserAvatar(Guid userId);
	}
}