﻿using System;
using System.Collections.Generic;
using Elevator.PhotoJunk.Data.Core.Dto;

namespace Elevator.PhotoJunk.Data.Core.Repositories
{
	public interface IAlbumRepository
	{
		Album Create(Album album);
		void Update(Album album);
		void Delete(Guid id);

		Album GetById(Guid id);
		Album GetByTitle(string title);
		IEnumerable<Album> GetAll();

		IEnumerable<Album> GetPage(int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Album> FilterByAuthor(Guid userId, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Album> Filter(string keyWords, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Album> Filter(string title, string descriprion, string authorName, int pageNumber, int itemsPerPage, out int itemsTotal);
	}
}