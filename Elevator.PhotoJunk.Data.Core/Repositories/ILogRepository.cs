﻿namespace Elevator.PhotoJunk.Data.Core.Repositories
{
	using System;

	public interface ILogRepository
	{
		void WiteToLog(string level, DateTime timeStamp, string source, string message);
	}
}