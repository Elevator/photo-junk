﻿namespace Elevator.PhotoJunk.Data.Core.Repositories
{
	using System;

	public interface IVoteRepository
	{
		void Vote(Guid userId, Guid photoId, int vote);
		int GetPositiveVotesForPhoto(Guid photoId);
		int GetNegativeVotesForPhoto(Guid photoId);
		int GetVote(Guid userId, Guid photoId);
	}
}