﻿using System;
using System.Collections.Generic;
using Elevator.PhotoJunk.Data.Core.Dto;

namespace Elevator.PhotoJunk.Data.Core.Repositories
{
	public interface IPhotoRepository
	{
		Photo Create(Photo photo);
		void Update(Photo photo);
		void Delete(Guid id);

		Photo GetById(Guid id);
		IEnumerable<Photo> GetAll();

		IEnumerable<Photo> GetPage(int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Photo> FilterByAuthor(Guid userId, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Photo> FilterByAlbum(Guid albumId, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Photo> FilterByKeywords(string keywords, int pageNumber, int itemsPerPage, out int itemsTotal);
		IEnumerable<Photo> Filter(string title, string descriprion, string place, string camera, string authorName, int pageNumber, int itemsPerPage, out int itemsTotal);

		void AddPhotoToAlbum(Guid photoId, Guid albumId);
		void RemovePhotoFromAlbum(Guid photoId, Guid albumId);
	}
}