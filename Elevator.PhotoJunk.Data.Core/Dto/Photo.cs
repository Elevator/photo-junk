﻿using System;

namespace Elevator.PhotoJunk.Data.Core.Dto
{
	public class Photo
	{
		public Guid IdPhoto { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Place { get; set; }
		public string Camera { get; set; }
		public double FocalLength { get; set; }
		public double ApertureSize { get; set; }
		public int IsoSpeed { get; set; }
		public bool Flash { get; set; }
		public Guid IdAuthor { get; set; }
		public Guid IdImage { get; set; }
		public DateTime UploadDate { get; set; }
	}
}