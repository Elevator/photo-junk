﻿using System;

namespace Elevator.PhotoJunk.Data.Core.Dto
{
	public class Image
	{
		public Guid IdImage { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public string MimeType { get; set; }
		public string FileExtension { get; set; }
	}
}