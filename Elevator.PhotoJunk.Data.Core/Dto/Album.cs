﻿using System;

namespace Elevator.PhotoJunk.Data.Core.Dto
{
	public class Album
	{
		public Guid IdAlbum { get; set; }
		public Guid IdAuthor { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public Guid IdCover { get; set; }
		public DateTime UploadDate { get; set; }
	}
}