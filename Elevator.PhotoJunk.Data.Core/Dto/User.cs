﻿namespace Elevator.PhotoJunk.Data.Core.Dto
{
	using System;

	public class User
	{
		public Guid IdUser { get; set; }
		public string Email { get; set; }
		public byte[] Password { get; set; }
		public string Nickname { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleName { get; set; }
		public string Location { get; set; }
		public string Description { get; set; }
		public DateTime RegistrationDate { get; set; }
		public bool IsPremium { get; set; }
	}
}